#amk 2

;SMWCP2 Watery Reef Map - MVS

#SPC
{
	#author "MVS"
	#comment "Water map theme"
	#game "SMW Central Production 2"
}

#pad $0468

#samples
{
	#default
	#AMM
	#main_map
}

w170 t48

#5 @13 q7f v200 y17


$EF $FF $5A $5A
$F1 $04 $3C $01
o3
[>c4c8<g8>c8<g8>c4
c8<g8>c8<g8a+4a+8f8
a+8f8a+4a+8f8a+8f8]6;




#6 @0 q7f y3


o3 $ED $5A $E5
[>v170c4v150c4v130c4v110c4v90c4v70c4
<v170a+4v150a+4v130a+4v110a+4v90a+4v70a+4
v170g4v150g4v130g4v110g4v90g4v70g4
v170a+4v150a+4v130a+4v110a+4v90a+4v70a+4]3;




#0 @1 q7f v225 y10

[r1]6
o5
c4c8<g8>c8<g8>c4
d4d+4<a+4a+8f8
a+8f8a+4>c4d4
<g4g8d8g8d8g4
a+4>c4<a+1^4^8
r8
>c4c8d8d+4c4
c4c4<a+4a+8>c8
d4<a+4a+4a+4
g4g8a+8>c4<g4
g4g4a+1^4^8r8;




#1 @1 q7f v200 y10


[r1]6
r8o5
c4c8<g8>c8<g8>c4
d4d+4<a+4a+8
f8a+8f8a+4>c4d4
<g4g8d8g8d8g4
a+4>c4<a+1^4^8
r8>c4c8d8d+4c4
c4c4<a+4a+8
>c8d4<a+4a+4a+4
g4g8a+8>c4<g4
g4g4a+1^4^8;




#2 q7f v140 y10


[r4o3@5c4o5@2c32a+32>c8^16r4
o3@5c4o5@2c32a+32>c8^16r4o2@5a+4
o4@2a+32o5@2g+32a+8^16r4o2@5a+4o4@2a+32o5@2g+32a+8^16
r4o3@5c4o4@2g32o5@2f32g8^16r4
o3@5c4o4@2g32o5@2f32g8^16r4o3@5d4
o4@2a+32o5@2g+32a+8^16r4o3@5d4o4@2a+32o5@2g+32a+8^16]3;




#3 q7f v130 y10

$DE $06 $0B $34
("Choir.brr", $04) $ED $28 $A6 ;Choir Aahs (28)
o4
[c1^2
<a+1^2
>c1^2
d2^4
c2^4]3;




#4 q7f y10
o3[@22v200f+8@22v180f+8@22v160f+8@22v140f+8@22v120f+8@22v100f+8]24;

#7 q7f v100 y10

("CLava Bubble.brr", $03) $ED $7F $E0 ;Drop sounds (2A)
o5[c2]38;