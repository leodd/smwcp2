;made by Torchkas
#amk 2

#SPC
{
	#title "Credits Theme"
	#game "SMW Central Production 2"
	#author "Torchkas"
	#comment "5 years"
}

#samples {
	#default
	"0c1.brr"
	"0c2.brr"
	"0c3.brr"
	"0c4.brr"
}

#instruments
{
	@6			$9A $A7 $00 $03 $00 ;@30
	@8			$AE $80 $00 $1E $00 ;@31
	@1			$8F $E0 $00 $06 $00 ;@32
	@21			$8F $E0 $00 $03 $20 ;@33
	@29			$8F $F3 $00 $03 $20 ;@34
	@13			$00 $00 $DF $03 $00 ;@35
	"0c1.brr"	$00 $00 $DF $04 $0C ;@36
	"0c2.brr"	$00 $00 $DF $04 $0F ;@37
	"0c3.brr"	$00 $00 $DF $04 $00 ;@38
	"0c4.brr"	$00 $00 $DF $04 $0E ;@39
	@22			$00 $00 $7F $05 $80 ;@40
	@9			$AF $B0 $00 $02 $A6 ;@41
	@10			$BF $D0 $00 $04 $00 ;@42
	@13			$8A $E0 $00 $06 $00 ;@43
	@1			$BF $80 $00 $06 $00 ;@44
	@9			$8F $E0 $00 $02 $A6 ;@45
}

(!130)[$ED $80 $B5]
(!131)[@35]
(!132)[@37]
(!133)[@36]
(!134)[@39]
(!135)[@38]

#0 w230 t43 $F4 $02
y9v165@30$DE $1B $14 $40
r2^8.
o5g2^16^24r48g8b12g6
f2^16r48<b24>d12f24a+8$DD $00 $10 b g12d24
e4^16^24r48d12e24>d16r16c16r16<g12d24b12
a+4^48r48>e12<a+12g12>$F4 $01 c4$E8 $30 $8C <b48a+48a48g48f48e48d48c48<a+48g48e48c96$F4 $01 ^96
y11v165
(2)[r8o5f+8$DD $00 $10 g b12g24r12g24>d12c24<b12f+24g24r24f+24g24<b24>d24
f8a24f24c24e12f24e12r24d12f24d24r24c24r12<a24>f+8$DD $00 $10 g
r8 <b24^24$DD $00 $02 g^24$DD $00 $02 >c^12$DD $00 $02 dr24e12d24g24r12d12c24]$ED $1A $A0<b12^24$DD $00 $02 g ^8$DD $00 $02 a
^4$DD $00 $02 f^4$DD $00 $02 e^12$DD $00 $02 d ^48@30f+48g12b24g12a24>c12$DD $00 $10 dq75y9c8$DD $00 $10 d
y10v185@41$DF
q7fr24
o4b12>d8d6f12g8<b8b24>f12^48
c16c16c16c12c12c8d+24f24r24q78f24q7ff24r24e8c24<g12
>c6g12d8d6a12g24f+12g12r24d24<b12>c6<b8>c12d8d+24f24r24q78f24q7fe24r24<b24g12f+24g8
r1.
v155@30
q5fo5g+12d12f12<b12f12g+12
y11v170@43$F4 $01
(9)[q7fo3c8$DD $00 $10 >c >c12<c24f+12g12r24a+24>c12r24<a+12>c12r24<a+24f24d+12
<a+16r16>a+12g8f24g12a+24>c12<g24f+12d+24c12f8r24
d+12c12r24d+24c24d+24f24g12r24a+24r24a+24r12g24a+24g24a+24>c16$F4 $01^16$F4 $01
f+8$DD $00 $10 g r12f+24f12d+24c12<a+24>c4<f+12g8]
g12v95(9)
<c24>g24r8^24
$F4 $01r1..
y10v160@45
$E3 $18 $33
o3d+24a+24>d+24
$EF $FF $64 $64
$F1 $00 $00 $01
$F5 $F1 $DB $E4 $E1 $E9 $F3 $F1 $F6
$F6 $0C $00 $F6 $1C $00
y8v145(10)[a+8g8.r16g12d+24f12g12r24d+24f12r24>c8
<a+8r8d12g6f8f8d+8]
$E3 $D8 $37
y10v155d+24a+24>d+24
a+8g8r8g12d+24f12g12r24d+24c12^48r48>c8
$E3 $C0 $3A
<a+8g8r12>d8c8<g+24g12g+8g24d+12f24
[r1]99

#1
y11v165@30$DE $1B $14 $40
r2^8.
o5d2^16^24r48d8d12d6
d2^16r48<g24b12>d24f+8$DD $00 $10 g f12<b24
>d4^16^24r48<b12b24>b16r16g16r16d12<b24>g12
g4^48r48>c12<g12e12a+2
y9v85
r12(2)$ED $1A $A0o4b12^24$DD $00 $02 g^24$DD $00 $02 a
y11v130^4$DD $00 $02 >f^4$DD $00 $02 e^8$DD $00 $02 dy9v85^24$DD $00 $01 <d ^48@30f+48g12b24g24y11v130>f12$DD $00 $10 gq75y9f8$DD $00 $10 g
y10v185@41$DF
q7fr8
o4g8g8b24b12b8g8f12g24
a24^64a16a16a16a48^64^96f12a8f24>c24r24q78c24q7fc24r48q7bd+24q7fc12^48<g24e12
g6b12g8g8b24>d12d24<b12>d12r24<g24g12
a8d24e8f12a8f24>c24r24q78c24q7fc24r24<f24e12e6
r1.
y11v155@30
q5fo5d12<b12>d12<f12d12f12
y9v110@43$F4 $01
q7fr24(9)
v150(9)
v110o3c24v150>g24r6^24
$F4 $01r1...
y12v145@45$EE $40
(10)
y11v155r8
(12)[o4a+8g8r8g12d+24f12g12r24d+24c12^48r48>c8
<a+8g8r12>d8c8<g+24g12g+8g24r12f24]
[r1]99

#2
y11v160@30$DE $1B $14 $40
r2^8.
o4b2^16^24r48b8g12b6
b2^16r48d24g12b24>c+8$DD $00 $10 d d12<g24
b4^16^24r48g12g24>e16r16e16r16<b12g24>e12
e4^48r48a+12e12c12g2
y10v225@35$DF $F4 $01 (!130, 1, 96)
[(!132, -1)r12o3b6b8b12r8b12$F4 $01^12$F4 $01(!133, -1)>d24$F4 $01^24$F4 $01(!132, -1)<b12a+12
r12a6a8a12(!134, -1)r24a8^12a16$F4 $01^16$F4 $01(!135, -1)g8g24]4
(!132, -1)r12o3b6b8b12r8b12$F4 $01^12$F4 $01(!133, -1)>d24$F4 $01^24$F4 $01(!132, -1)<b12b24$F4 $01^24
(!133, -1)>f16r48f16r48f16r48(!132, -1)b12^8$DD $00 $18 <b r24
(!130, 0) (!132, 0)
y9v155@30
q5fo4b12g+12g+12d12d12<b12
y10v145$DE $1B $14 $40
[q7fo4g4^12r24g12f12r24f12r24c24f24r24g+12r24d+24
g8^12r24f24r24d24f12d+4d24d+12f6]4
o4g4^12r24g12f12r24f12r24c24f24r24g+12r24d+24
g8^12r24f24r24d24f12d+4d24d+12f6^8
y8v145@45
(11)[o3d+4<a+8^12>f4^24c4
g4d8^12c4r24<g+4
>d+4<a+4>f4c4
g4d4g+4c4]
[r1]99

#3
y10v168@30$DE $1B $14 $40
r2^8.
o4g2^16^24r48g8d12g6
g2^16r48<b24>d12g24a+8$DD $00 $10 b b12d24
g4^16^24r48e12e24b16r16>d16r16<e12e24>d12
c4^48r48g12<a+12g12>e2
y10v185@35$DF $F4 $01 (!131, -1) (!130, 1, 192)
[r12o4d6d8d12r8d8^48q7ba+12^48q7fd12c+12
r12c6c8c12r24a12f8e8e24g24r24e24]4
r12o4d6d8d12r8d8^48q7ba+12^48q7fd12b12
b16r48b16r48b16r48>d12^8$DD $00 $18 <d r24
$F4 $01
r2(!130, 0) (!131, 0)
y11v140@30$DE $1B $14 $40
[o4d+4^12r24d+12d+12r24d+12r24d+12r24d+12r24c24
f8^12r24d24r24<a+8>c4<a+24>c4]4
o4d+4^12r24d+12d+12r24d+12r24d+12r24d+12r24c24
f8^12r24d24r24<a+8>c4<a+24>c4.
y12v145@45$EE $60
(11)
[r1]99

#4
y10v240@31$FA $03 $50
r2^8.
o1g1 ^2$DD $00 $0C >g ^1$DD $00 $0C e ^2
^16$DD $00 $0C c ^24r48c16^24r48c16^24r48c12
$F4 $01 <a+24>>c24<e24g24a+24e24<a+24>c24<g24a+24>c16$F4 $01 ^16
[o1g8r12g8r24g12d24g12r24g12b24g12g12f+12
f8r12f8r24f12>c24<f12r24f12r24>c12<e12c12]4
g8r12g8r24g12d24g12r24g12b24g12g12>d12
g16r48g16r48g16r48b12^8$DD $00 $18 <b g+24b12>g+12f12<f12>d12<b12
[o2c8>c12<a+8d+24d12d+8f8c24<f12g+12r24g+24
g12r24>g12f24d8<g12g+6>g+8^12g6]5
d+8$DD $00 $16 >d+
r8
y10v150@45
r4.r12o4f12r24c24d+12r24f8
r8g6r6^8d+24r12<b24r8r48>g24>d24g24
r2^12^48<d+12r6d+6
r12f8c12r24f6r4^24<a+24r8
[r1]99

#5
y11v135@32$F4 $01
r2^8. l48
(1)[o5g24dg<b>d<gbdg<b>d<gbgb>d<b>gdbg>d<b>g<b>dg<b>d<gbdg<b>d<gbgb>d<b>gdbg>d<b>
f<b>df<b>d<gbfg<b>f<gbgb>f<b>gfbg>d<b>f<b>df<b>d<gbfg<b>f<gbgb>f<b>gfbg>d<b>
e<b>de<b>d<gbegbe<b>d<b>dedgebg>d<b>e<b>de<b>d<gbegbe<b>d<b>dedgebg>d<b>
e<a+>ce<a+>c<ga+egce<g>c<g>cecgea+g]o5c<a+>e<a+>ce<a+>c<ga+egce<g>c<g>cecgea+g>c<a+
[q79>g24dg<b>d<gbdg<b>d<gbgb>d<b>gdbg>d<b>g<b>dg<b>d<gbdg<b>d<gbgb>d<b>gdbg>d<b
>f<a>cf<a>c<facf<a>c<fafa>c<a>fcaf>c<a>f<a>cf<a>c<facf<a>c<egeg>c<g>ecge>c<g]4
>g24dg<b>d<gbdg<b>d<gbgb>d<b>gdbg>d<b>g<b>dg<b>d<gbdg<b>d<gbgb>d<b>gdbg>d<b
g>d<bg>d<b>fdgfbg<g>b<bg>d<b>fdgfbgg+24b24f24g+24d24f24<b24>d24<g+24b24f24g+48$F4 $01^48
y9v140@30$DE $1B $14 $40
[q7fo3a+4^12r24a+12g+12r24g+12r24g+12r24g+12r24g+24
a+8^12r24a+24r24g8g+4f24g+4]4
o3a+4^12r24a+12g+12r24g+12r24g+12r24g+12r24g+24
a+8^12r24a+24r24g8g+4f24g+4.
r8
y12v150@45$EE $20
o4d+6r12d+12r8d+12r12c8d+8
r8f6r12f8r12c24r8c8^12r6
<a+6r12a+6r12>c8r8c6
r12d8r8d6r12d+6r12d+8
[r1]99

#6
y9v90@32$F4 $01
r2^8.
r12(1)o5c96$F4 $01 ^96 y11v200@34$FA $03 $40 q7bo4d+24.q7ad+12 y9v90@32$FA $03 $00 $F4 $01 q7fga+egce<g>c96$F4 $01 ^96 y11v190@34$FA $03 $40 q7cd+8
y10v180@40
(3)[q7fc8q7cc12q79c24q7fc8q7cc12q79c24q7fc8q7cc12q79c24q7fc12q77c12q7ac24q7dc24]3
(5)[q7fc8q7cc12q79c24q7fc8q7cc12q79c24q7fc12q7cc12q79c12q7fc12q7cc24q78c24q7cc24q78c24]
(3)3
(5)
(3)
q7fc8q7cc12q79c24q7fc8q7cc12q79c24q7fc12q78c12q7cc12q7fc12q78c12q7cc12
(99)[r2]8
y11v150@44$DE $00 $13 $2F $F4 $01
q7fr8o5d+24<b24>c6<g12r24f12a+12r24f24g12r24g+8
g16$F4 $01^16$F4 $01>g12f12$F4 $01^12$F4 $01>c12<a+24f+12d+24f12d+48$F4 $01^48$F4 $01c12<a+8b24
>c16$F4 $01^16$F4 $01<a+12g12r24f+48g24r24f+16r48f12^48a+12>d+16r16<a+24>c12d+24
f12f+24c12d+8<g24d+12a+24>c12<d+24g+8^12g8v80c24
r1^1$F4 $01
y10v160@45
r48o3g24>d24g24
r12^48
y8v150@45$EE $10
o3a+6r12a+6r24g+12r12g+12r24g+8
r8a+6r12a+8r12g+24r8g+8^12r6
g6r12g6r12g+6r12g+6
r12a+8r8a+6r12>c6r12c8
[r1]99

#7
y10v255@33$FA $03 $40 h3
r2^8.
[o4c4]12
c8c8c8c8
c12y11v200@34q7fc12.q7ec24
y10v255@33c12y11v190@34q79c12.q7fc24
v255
(4)[y10@33q7fc8c8y11@34q79c8y10@33q7dc12c8y11@34q79c24y10@33q7dc8y11@34q79c8y10@33q7dc8]3
(6)[y10@33q7fc8c8y11@34q79c12y10@33q7fc24c12y11@34q79c24y10@33q7fc12y11@34q7ac24q76c12q79c24y10@33q7dc12y11@34q78c24q76c12q7ac24]
(4)3
(6)
(4)
y10@33q7fc8c8y11@34q79c8y10@33q7fc12q7dc24q7fc12@34$EB $00 $10 $7A y8q79<a12y12f12y10@33$EB $00 $00 $00 q7f>c12@34$EB $00 $10 $7A y8q79<a12y12f12
$EB $00 $00 $00
(7)[y10@33q7f>c8@40h0q78c12q76c24y9@42h3q7ac8y10@40h0q78c12q76c24@33h3q7fc8@40h0q78c12q76c24y9@42h3q79c12y10@40h0q75c24q79c24q76c24q78c24]3
(8)[y10@33h3q7fc8@40h0q78c12q76c24y9@42h3q7ac8y10@40h0q78c12q76c24@33h3q7fc8@40h0q78c12q76c24y9@42h3q79c12q75c24q78c12q7ac24]
(7)3
(8)
(7)
y10@33q7fc8@40h0q78c12q76c24y9@42h3q7ac8y10@40h0q78c12q76c24@33h3q7fc12q7dc24@40h0q78c12y9@42h3q7ac24y10@33q7fc12y9@42q7ac24y11<a+12y9f+24y14>d+24y12<d+24y8b6
r1^2..
y9v155@45$FA $03 $00
(12)
[r1]99