#jsr changePause

#asm changePause
{
	mov	a, $038C
	bne	.return
	mov	a, $0387		; \
	mov	!SpeedUpBackUp, a	; | Set the tempo increase to 0 and save it.
	mov	a, #$00			; |
	mov	$0387, a		; /

	inc	a
	mov	!PauseMusic, a
	mov	$f2, #$5c		; \ Key off voices
	mov	$f3, #$ff		; /
.return
	ret
}
