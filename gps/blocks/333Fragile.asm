;Fragile Block, ASM style
;08/16/2009 (Gonna be 19 in 11 days!)

db $42
JMP Return : JMP MarioAbove : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP MarioAbove : JMP Return : JMP Return
;These determine what code to run based on how the block is being touched. They are best left like this.

MarioAbove:
LDA #$02 	; Replace block with blank tile
STA $9C
JSL $00BEB0
PHB
LDA #$02
PHA
PLB
LDA #$00 	;set to 0 for normal explosion, 1 for rainbow (throw blocks)
JSL $028663 	;breaking effect
PLB
Return:
RTL ;Do nothing if mario isn't coming from above

;I didn't write this code.
;See http://www.smwcentral.net/?p=thread&pid=296475&r=1#id296475 for where I got it
;(pg 99 of the ASM help thread if link is broken, bout half-way down the page)