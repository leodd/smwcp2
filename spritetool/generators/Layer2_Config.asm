PRINT "INIT ",pc
	RTL
PRINT "MAIN ",pc
	PHB
	PHK
	PLB
	JSR GEN1
	JSR GEN2
	PLB
	RTL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; "Customizable BG scrolling rate" generator
; Coded by Jimmy52905
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SETUP
; Read the Readme for more info.
; It's set to H-Scroll: "Variable", V-Scroll: "Slow" by default!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	!HorizLayer2_SpeedSlow = $07		;Change this to a higher value to make Layer 2 scroll slower horizontally. Set to a value from $01 to $07.
	!HorizLayer2_SpeedFast = $01		;Change this to a higher value to make Layer 2 scroll faster horizontally. Set to a value from $01 to $07.

	!VertLayer2_SpeedSlow = $06		;Change this to a higher value to make Layer 2 scroll slower vertically. Set to a value from $01 to $07.
	!VertLayer2_SpeedFast = $01		;Change this to a higher value to make Layer 2 scroll faster vertically. Set to a value from $01 to $07.

	!BG_INITPostionHi = $00			;\
	!BG_INITPostionLo = $8A			;/ Set this to the BG initial position you want. Setting it in LM won't work!

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; MAIN CODE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
GEN1:
MAIN1:
	STZ $1413			;Set horizontal BG scrolling rate to "none" so that this code will work properly.
	STZ $1414			;Set vertical BG scrolling rate to "none" so that this code will work properly.

	REP #$20

	LDA $1462
	LDX #!HorizLayer2_SpeedFast
LOOP1:
	ASL A
	STA $1466
	DEX
	CPX #$00
	BNE LOOP1

	LDX #!HorizLayer2_SpeedSlow
LOOP2:
	LSR A
	STA $1466
	DEX
	CPX #$00
	BNE LOOP2

	LDA $1464
	LDX #!VertLayer2_SpeedFast
LOOP3:
	ASL A
	STA $1468
	DEX
	CPX #$00
	BNE LOOP3

	LDX #!VertLayer2_SpeedSlow
LOOP4:
	LSR A
	STA $1468
	DEX
	CPX #$00
	BNE LOOP4

	LDX #!BG_INITPostionLo
INITLOOPLO:
	INC $1468
	DEX
	CPX #$00
	BNE INITLOOPLO

	LDX #!BG_INITPostionHi
INITLOOPHI:
	INC $1469
	DEX
	CPX #$00
	BNE INITLOOPHI

	SEP #$20
	RTS










;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; "Vertical Auto Scroll" generator
; By Jimmy52905
; Fixed by yoshicookiezeus
; Uses first extra bit: YES
; If extra bit is set, the generator makes the screen scroll downwards instead of upwards
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

STOP:		db $10		; screen number the sprite should STOP at when scrolling downwards

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Main sprite code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GEN2:
MAIN2:
	LDA $9D		;\ If sprites are locked...
	BNE RETURN	;/ Go to "RETURN".

	LDA $13		;\
	LSR A		; |
	BCC RETURN	;/ Only run code every other frame.

	STZ $1412	; Disable Vertical Scroll so that the code can run properly.

	LDA $18B9	;\ If the first extra bit is set...
	AND #$40	; |
	BNE DOWN	;/ Go to "DOWN".

	STZ $55		; Make sure that new tiles are uploaded from the top of the screen

	LDA $1464	;\
	ORA $1465	; |If the screen position is at the top of the level...
	BEQ RETURN	;/ Go to "RETURN".

	REP #$20	; 16-bit (accumulator).
	DEC $1464	; Make the screen scroll upwards automatically.
	SEP #$20	; 8-bit (accumulator).

RETURN:
	RTS		;RETURN from Subroutine.

DOWN:
	LDA #$02	;\ Make sure that new tiles are uploaded from the bottom of the screen
	STA $55		;/

	LDA $1465	;\ If the screen position is at screen 10 (last screen of a vertical layer 2 level)...
	CMP STOP	; |
	BEQ RETURN	;/ Go to "RETURN".

	REP #$20	; 16-bit (accumulator).
	INC $1464	; Make the screen scroll downwards automatically.
	SEP #$20	; 8-bit (accumulator).

	RTS		;RETURN from Subroutine.
	