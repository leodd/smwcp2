;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Laser Orb Switch, by imamelia
;;
;; This is a switch that points a certain direction, changes direction when hit, and
;; redirects a specific custom sprite (the laser orb) to move in that direction if it
;; comes into contact.
;;
;; Uses first extra bit: YES
;;
;; If the extra bit is clear, the sprite will use either the four cardinal or diagonal
;; directions depending on its X position.  If the extra bit is set, it will use all eight
;; main directions.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; defines and tables
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!BlockTile = $80
!BlockPalette = $0F
ArrowTiles:
db $82,$84,$86,$84,$82,$84,$86,$84
ArrowTileProps:
db $0B,$0B,$0B,$4B,$4B,$CB,$8B,$8B
YOffset:
db $00,$FC,$F9,$F8,$F8,$F9,$FC,$00,$00

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc

LaserOrbSwitchInit:

LDA $D8,x			;
SEC					;
SBC #$01				;
STA $D8,x			;
LDA $14D4,x			;
SBC #$00				;
STA $14D4,x			;

LDA $7FAB10,x	;
AND #$04		; if the extra bit is clear...
BNE .Continue		;
LDA $E4,x		;
AND #$10		; and the sprite is on an odd X-coordinate...
BEQ .Continue		;
INC $151C,x		; make it start out facing up-left instead of straight left
.Continue			;

LDA $D8,x		;
CLC				;
ADC #$08		; shift the sprite down 8 pixels
STA $D8,x		;
LDA $14D4,x		;
ADC #$00		;
STA $14D4,x		;

RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR LaserOrbSwitchMain
PLB
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

LaserOrbSwitchMain:

JSR LaserOrbSwitchGFX

LDA $14C8,x
CMP #$08
BNE Return00
LDA $9D
BNE Return00

JSR SubOffscreenX0

STZ $1594,x			;

LDA $C2,x			;
JSL $8086DF			;

dw S00_Stationary		;
dw S01_JustHit		;
dw S02_Moving		;

Return00:
RTS

S00_Stationary:
JSL $81B44F			;
RTS					;

S01_JustHit:
LDA #$0B				;
STA $1DF9			;
LDA $7FAB10,x		;
EOR #$04				;
LSR #3				;
LDA $151C,x			;
ADC #$01			;
AND #$07			;
STA $151C,x			;
INC $1558,x			;
INC $C2,x			;
RTS					;

S02_Moving:
LDA $1558,x			;
LSR					;
TAY					;
LDA YOffset,y			;
STA $160E,x			;
ROL $1594,x			;
LDA $1558,x			;
CMP #$01			;
BNE .End				;
STZ $C2,x			;
.End					;
RTS					;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; graphics routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

LaserOrbSwitchGFX:

LDA $1594,x			;
BNE .Return			;

JSR GetDrawInfo

LDA $00				;
STA $0300,y			;
STA $0304,y			;

LDA $01				;
CLC					;
ADC $160E,x			;
STA $0301,y			;
STA $0305,y			;

LDA $151C,x			;
TAX					;
LDA.w ArrowTiles,x		;
STA $0302,y			;
LDA.w ArrowTileProps,x	;
ORA $64				;
STA $0303,y			;

LDA #!BlockTile		;
STA $0306,y			;
LDA #!BlockPalette		;
ORA $64				;
STA $0307,y			;

LDX $15E9			;
LDY #$02				;
LDA #$01			;
JSL $81B7B3			;
.Return				
RTS					;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; miscellaneous standard subroutines
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Table1:              db $0C,$1C
Table2:              db $01,$02
Table3:              db $40,$B0
Table6:              db $01,$FF
Table4:              db $30,$C0,$A0,$C0,$A0,$F0,$60,$90,$30,$C0,$A0,$80,$A0,$40,$60,$B0
Table5:              db $01,$FF,$01,$FF,$01,$FF,$01,$FF,$01,$FF,$01,$FF,$01,$00,$01,$FF

SubOffscreenX0:
LDA #$00
;BRA SubOffscreenMain
;SubOffscreenX1:
;LDA #$02
;BRA SubOffscreenMain
;SubOffscreenX2:
;LDA #$04
;BRA SubOffscreenMain
;SubOffscreenX3:
;LDA #$06
;BRA SubOffscreenMain
;SubOffscreenX4:
;LDA #$08
;BRA SubOffscreenMain
;SubOffscreenX5:
;LDA #$0A
;BRA SubOffscreenMain
;SubOffscreenX6:
;LDA #$0C
;BRA SubOffscreenMain
;SubOffscreenX7:
;LDA #$0E

SubOffscreenMain:

STA $03

JSR SubIsOffscreen
BEQ Return2

LDA $5B
LSR
BCS VerticalLevel
LDA $D8,x
CLC
ADC #$50
LDA $14D4,x
ADC #$00
CMP #$02
BPL EraseSprite
LDA $167A,x
AND #$04
BNE Return2
LDA $13
AND #$01
ORA $03
STA $01
TAY
LDA $1A
CLC
ADC Table4,y
ROL $00
CMP $E4,x
PHP
LDA $1B
LSR $00
ADC Table5,y
PLP
SBC $14E0,x
STA $00
LSR $01
BCC Label20
EOR #$80
STA $00
Label20:
LDA $00
BPL Return2

EraseSprite:
LDA $14C8,x
CMP #$08
BCC KillSprite
LDY $161A,x
CPY #$FF
BEQ KillSprite
LDA #$00
STA $1938,y
KillSprite:
STZ $14C8,x
Return2:
RTS

VerticalLevel:

LDA $167A,x
AND #$04
BNE Return2
LDA $13
LSR
BCS Return2
AND #$01
STA $01
TAY
LDA $1C
CLC
ADC Table3,y
ROL $00
CMP $D8,x
PHP
LDA $1D
LSR $00
ADC Table6,y
PLP
SBC $14D4,x
STA $00
LDY $02
BEQ Label22
EOR #$80
STA $00
Label22:
LDA $00
BPL Return2
BMI EraseSprite

SubIsOffscreen:
LDA $15A0,x
ORA $186C,x
RTS

GetDrawInfo:

STZ $186C,x
STZ $15A0,x
LDA $E4,x
CMP $1A
LDA $14E0,x
SBC $1B
BEQ OnscreenX
INC $15A0,x
OnscreenX:
LDA $14E0,x
XBA
LDA $E4,x
REP #$20
SEC
SBC $1A
CLC
ADC.w #$0040
CMP #$0180
SEP #$20
ROL A
AND #$01
STA $15C4,x
BNE Invalid

LDY #$00
LDA $1662,x
AND #$20
BEQ OnscreenLoop
INY
OnscreenLoop:
LDA $D8,x
CLC
ADC Table1,y
PHP
CMP $1C
ROL $00
PLP
LDA $14D4,x
ADC #$00
LSR $00
SBC $1D
BEQ OnscreenY
LDA $186C,x
ORA Table2,y
STA $186C,x
OnscreenY:
DEY
BPL OnscreenLoop
LDY $15EA,x
LDA $E4,x
SEC
SBC $1A
STA $00
LDA $D8,x
SEC
SBC $1C
STA $01
RTS

Invalid:
PLA
PLA
RTS



