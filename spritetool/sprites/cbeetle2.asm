incsrc "subroutinedefs_xkas.asm"

!x_speed = $08
!x_speed_ground = $21

!distance = $28

!tile1 = $80	;first frame, walking
!tile2 = $82	;second frame, walking
!tile3 = $84	;first frame, in shell
!tile4 = $86	;second frame, in shell
!tile5 = $88	;third frame, in shell

print "INIT ",pc
	JSL !SubHorizPos
	TYA
	STA $157C,x
	STZ $1510,x
	LDA $D8,x
	SEC
	SBC #$02
	STA $D8,x
	LDA $14D4,x
	SBC #$00
	STA $14D4,x
	RTL

print "MAIN ",pc
	PHB
	PHK
	PLB
	JSR main
	PLB
	RTL

main:
	JSR Graphics
	LDA $14C8,x
	EOR #$08
	ORA $9D
	BNE .return
	
	JSL !SubOffScreenX0
	
	JSL $81A7DC
	JSL $819138		; mario interaction
	JSL $818032		; sprite interaction
	
	LDA $1588,x
	AND #$03
	BEQ .skip_flip
	LDA $157C,x
	EOR #$01
	STA $157C,x
.skip_flip	

	LDA $1588,x
	AND #$04
	BEQ .not_on_ground
	
	LDA #$11		; spawn buzzy beetle
	STA $9E,x
	LDA #$0A
	STA $14C8,x
	JSL $87F7D2
	
	JSL !SubHorizPos
	TYA
	STA $157C,x
	LDA .x_speed_ground,y
	STA $B6,x

	LDA $15F6,x
	ORA #$80
	STA $15F6,x

	; JSL $81802A		; x/y with gravity
.return
	RTS
	

.not_on_ground
	LDA $1510,x		; check if already falling
	BNE .falling
	
	LDA #!distance
	JSR proximity_x
	BEQ .not_falling
	LDA #$01
	STA $1510,x
	
.falling
	STZ $B6,x
	JSL $81802A		; x/y with gravity
	RTS
	
.not_falling
	LDY $157C,x
	LDA .x_speed,y
	STA $B6,x
	STZ $AA,x
	JSL $81801A		; y no gravity
	JSL $818022		; x no gravity
	RTS

.x_speed
	db !x_speed,-!x_speed

.x_speed_ground
	db !x_speed_ground,-!x_speed_ground

Graphics:
	JSL !GetDrawInfo
	
	LDA $15F6,x
	ORA $64
	ORA #$80
	STA $02

	LDA $00
	STA $0300,y
	LDA $01
	STA $0301,y
	
	LDA $1510,x
	BNE .falling
	
	PHX
	LDA $14
	LSR
	LSR
	LSR
	AND #$03
	TAX
	LDA .tiles,x
	STA $0302,y
	PLX
	LDA $157C,x
	EOR #$01
	ROR #3
	ORA $02
	STA $0303,y
	BRA .end
	
.falling
	PHX
	LDA $14
	LSR
	LSR
	AND #$03
	TAX
	LDA .tiles_f,x
	STA $0302,y
	LDA $02
	ORA .x_flip,x
	STA $0303,y
	PLX
.end
	LDY #$02			; 16x16
	LDA #$00			; 1 tile
	JSL $81B7B3
	RTS


.tiles
	db !tile1,!tile2,!tile1,!tile2
.tiles_f
	db !tile3,!tile4,!tile3,!tile5
	
.x_flip
	db $00,$00,$40,$00

; check if the player is in proximity horizontally
; entry conditions:
;	A = distance
; exit conditions:
;	A = result (bool)
;	Y = direction (1 = left, 0 = right)
;	$00 = distance
proximity_x:
	STA $00
	STZ $01
	LDY #$01
	LDA $14E0,x
	XBA
	LDA $E4,x
	REP #$20
	SEC
	SBC $94
	BPL +
	DEY
	EOR #$FFFF
	INC
+	CMP $00
	STA $00
	SEP #$20
	BCC +
	LDA #$00			;???
	RTS
	
+	LDA #$01
	RTS
