;Pionpi
;by smkdan
;GFX from andyk250

;I lifted some of the code from the Rex since it's stomping behaviour can be applied here

incsrc subroutinedefs_xkas.asm

;some definitions
!DIR = $03
!PROPRAM = $04
!FRAMEINDEX = $09

;how long to stay knocked out
!KNOCKOUTTIME = $80
;how long between jumps
!JUMPINTERVAL = $40

;if extra bit is set, spin jumps / yoshi won't kill and it will just knockout as normal

;some ram
;1570:	state
;	00: living and standing (going to JUMP)
;	01: living and in air
;	02: knocked out
;1602:	general timer for above states

;states
!LIVESTAND = $00
!LIVEAIR = $01
!KNOCKOUT = $02

;speeds for jumping
!JUMPX = $20
!JUMPY = $20

PRINT "INIT ",pc
	JSR SUB_HORZ_POS	;face mario
	TYA
	STA $157C,x		;new direction

	LDA #!LIVESTAND		;standing state
	STA $1570,x
	LDA #!JUMPINTERVAL	;set timer for jumping interval
	STA $1602,x
	RTL

PRINT "MAIN ",pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
	RTL

RETURN_I:
	RTS

Run:
	JSR SUB_OFF_SCREEN_X0
	JSL !GetDrawInfo
	JSR GFX			;draw sprite

	LDA $14C8,x
	CMP #$08          	 
	BNE RETURN_I           
	LDA $9D			;locked sprites?
	BNE RETURN_I
	LDA $15D0,x		;yoshi eating?
	BNE RETURN_I

	JSL $01802A		;speed

;figure out what to do according to state
	LDA $1570,x		;load state
	BEQ DOLIVESTAND	;00
	DEC A
	BEQ DOLIVEAIR		;01
	DEC A
	BEQ DOKNOCKEDOUT		;02

DOLIVESTAND:
	DEC $1602,x		;decrement timer
	BEQ JUMP
	JMP INTERACTION
JUMP:
	JSR SUB_HORZ_POS	;first, face mario just before the JUMP
	TAY
	STA $157C,x		;no direction

	LDA #!JUMPX		;load jumpx speed
	LDY $157C,x		;load direction
	BEQ NOFLIPJ
	EOR #$FF		;invert bits
NOFLIPJ:	
	STA $B6,x		;set xspd

	LDA #!JUMPY		;load jumpy speed
	EOR #$FF		;invert
	STA $AA,x		;set yspd
	
	LDA #!LIVEAIR		;set in air status
	STA $1570,x

	JMP INTERACTION		;just JUMP to INTERACTION routine

DOLIVEAIR:

	LDA $1588,x		;test for wall contact
	AND #$01
	BEQ NORIGHTWALL

	LDA #$E0
	STA $B6,x
	;LDA $157C,x		;flip direction because it's touching a wall
	;EOR #$01
	LDA #$01
	STA $157C,x
NORIGHTWALL:

	LDA $1588,x		;test for wall contact
	AND #$02
	BEQ NOLEFTWALL
	LDA #$20
	STA $B6,x
	;LDA $157C,x		;flip direction because it's touching a wall
	;EOR #$01
	STZ $157C,x

NOLEFTWALL:

	LDA $1588,x		;test if on ground
	BIT #$04
	BNE ONGROUND

	BRA NOWALL

	LDA $157C,x		;flip direction because it's touching a wall
	EOR #$01
	STA $157C,x

;restore xspd according to new direction
	LDA #!JUMPX		;load jumpx speed
	LDY $157C,x		;load direction
	BEQ NOFLIPJA
	EOR #$FF		;invert bits
NOFLIPJA:	
	STA $B6,x		;set xspd

NOWALL:
	JMP INTERACTION	

ONGROUND:

	LDA #!LIVESTAND		;on ground
	STA $1570,x
	LDA #!JUMPINTERVAL
	STA $1602,x		;set JUMP interval in timer
	STZ $B6,x		;no xspd

DOKNOCKEDOUT:
	LDA $B6,x		;test xspd, slow down if not stopped already
	BEQ STILL
	BMI INCX
	DEC $B6,x		;going right, --
	BRA STILL

INCX:
	INC $B6,x		;going left, --

STILL:
	DEC $1602,x		;decrement timer
	BEQ LIVEAGAIN
	RTS			;just return

LIVEAGAIN:
	LDA #!LIVESTAND		;set status to live
	STA $1570,x
	LDA #!JUMPINTERVAL	;set JUMP interval in timer
	STA $1602,x

INTERACTION:
	JSL $01A7DC		;mario interact
	BCC NO_CONTACT          ; (carry set = mario/rex contact)
	LDA $1490               ; \ if mario star timer > 0 ...
	BNE HAS_STAR            ; /    ... goto HAS_STAR
	LDA $154C,x             ; \ if rex invincibility timer > 0 ...
	BNE NO_CONTACT          ; /    ... goto NO_CONTACT
	LDA $7D                 ; \  if mario's y speed < 10 ...
	CMP #$10                ;  }   ... rex will hurt mario
	BMI REX_WINS            ; /    

MARIO_WINS:          
	JSR SUB_STOMP_PTS       ; give mario points
	JSL $01AA33             ; set mario speed
	JSL $01AB99             ; display contact graphic
	LDA $140D               ; \  if mario is spin jumping...
	ORA $187A               ;  }    ... or on yoshi ...
	BNE SPIN_KILL           ; /     ... goto SPIN_KILL

;set state to knocked out
	LDA #!KNOCKOUT		;set knockout state
	STA $1570,x	
	LDA #!KNOCKOUTTIME
	STA $1602,x		;set time to be knocked out
	RTS                     ; return 

REX_WINS:            
	LDA $1497               ; \ if mario is invincible...
	ORA $187A               ;  }  ... or mario on yoshi...
	BNE NO_CONTACT          ; /   ... return
	JSR SUB_HORZ_POS         ; \  set new rex direction
	TYA                     ;  }  
	STA $157C,x             ; /
	JSL $00F5B7             ; hurt mario
	RTS

SPIN_KILL:
;first we need to check the extra bit
	LDA $7FAB10,x		;custom sprite properties
	AND #$04		;test extra bit
	BEQ PROCEEDSPINKILL

	JSL $01AA33             ; set mario speed
	JSL $01AB99             ; display contact graphic

;set state to knocked out
	LDA #!KNOCKOUT		;set knockout state
	STA $1570,x	
	LDA #!KNOCKOUTTIME
	STA $1602,x		;set time to be knocked out
	RTS                     ; return 
		
PROCEEDSPINKILL:     
	LDA #$04                ; \ rex status = 4 (being killed by spin JUMP)
	STA $14C8,x             ; /   
	LDA #$1F                ; \ set spin JUMP animation timer
	STA $1540,x             ; /
	JSL $07FC3B             ; show star animation
	LDA #$08                ; \ play sound effect
	STA $1DF9               ; /
	RTS                     ; return

NO_CONTACT:          
	RTS                     ; return

STAR_SOUNDS:         db $00,$13,$14,$15,$16,$17,$18,$19
KILLED_X_SPEED:      db $F0,$10

HAS_STAR:            
	LDA #$02                ; \ rex status = 2 (being killed by star)
	STA $14C8,x             ; /
	LDA #$D0                ; \ set y speed
	STA $AA,x               ; /
	JSR SUB_HORZ_POS         ; get new rex direction
	LDA KILLED_X_SPEED,y    ; \ set x speed based on rex direction
	STA $B6,x               ; /
	INC $18D2               ; increment number consecutive enemies killed
	LDA $18D2               ; \
	CMP #$08                ; | if consecutive enemies stomped >= 8, reset to 8
	BCC NO_RESET2           ; |
	LDA #$08                ; |
	STA $18D2  		; /   
NO_RESET2:           
	JSL $02ACE5             ; give mario points
	LDY $18D2               ; \ 
	CPY #$08                ; | if consecutive enemies stomped < 8 ...
	BCS NO_SOUND2           ; |
	LDA STAR_SOUNDS,y             ; |    ... play sound effect
	STA $1DF9               ; /

NO_SOUND2:           
	RTS                     ; final return      
			
;=====

TILEMAP:	db $8C,$84	;head, sitting frame
	db $80,$82	;head, jumping frame
	db $8E,$8E	;knock out frame x2

XDISP:	db $02,$00
	db $02,$00
	db $00,$00

YDISP:	db $F5,$03
	db $F4,$02
	db $FF,$FF
	
	
GFX:
	LDA $157C,x	;direction...
	STA !DIR

	LDA $15F6,x	;properties...
	STA !PROPRAM

	LDA !DIR		;test direction
	BNE NOFLIP
	LDA #$40
	TSB !PROPRAM	;set flip bit

NOFLIP:
	LDA $1570,x	;load state (and frame to use)
	ASL A		;x2
	STA !FRAMEINDEX	;convenient frame index

	LDX #$00	;reset loop index

OAM_LOOP:
	PHX		;preserve loop index
	LDX !FRAMEINDEX	;load frame index

	LDA !DIR		;test direction
	BEQ FLIPX

	LDA $00
	CLC
	ADC XDISP,x	;xpos
	STA $0300,y
	BRA DoY

FLIPX:
	LDA $00
	SEC
	SBC XDISP,x	;xpos flip
	CLC
	ADC #$04
	STA $0300,y

DoY:
	LDA $01
	CLC
	ADC YDISP,x	;ypos
	STA $0301,y

	LDA TILEMAP,x	;chr
	STA $0302,y

	LDA !PROPRAM	;properties
	ORA $64
	STA $0303,y

	INY		;next tile
	INY
	INY
	INY

	INX
	STX !FRAMEINDEX	;store to frame index

	PLX		;pull loop index
	INX
	CPX #$02	;2 tiles
	BNE OAM_LOOP

	LDX $15E9	;restore sprite index
	LDY #$02	;16x16
        LDA #$01        ;2 tiles
        JSL $01B7B3	;reserve

	RTS
	
;=================
;BORROWED ROUTINES
;=================

;POINTS
;======

SUB_STOMP_PTS:       PHY                     ; 
                    LDA $1697               ; \
                    CLC                     ;  } 
                    ADC $1626,x             ; / some enemies give higher pts/1ups quicker??
                    TAY                     ;
                    INY                     ;
                    CPY #$08                ; \ if consecutive enemies stomped >= 8 ...
                    BCS NO_SOUND            ; /    ... don't play sound 
                    LDA STAR_SOUNDS,y             ; \ play sound effect
                    STA $1DF9               ; /   
NO_SOUND:            TYA                     ; \
                    CMP #$08                ;  | if consecutive enemies stomped >= 8, reset to 8
                    BCC NO_RESET            ;  |
                    LDA #$08                ; /
NO_RESET:            JSL $02ACE5             ; give mario points
                    PLY                     ;
                    RTS                     ; return



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_OFF_SCREEN
; This subroutine deals with sprites that have moved off screen
; It is adapted from the subroutine at $01AC0D
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
SPR_T12:             db $40,$B0
SPR_T13:             db $01,$FF
SPR_T14:             db $30,$C0,$A0,$C0,$A0,$F0,$60,$90		;bank 1 sizes
		            db $30,$C0,$A0,$80,$A0,$40,$60,$B0		;bank 3 sizes
SPR_T15:             db $01,$FF,$01,$FF,$01,$FF,$01,$FF		;bank 1 sizes
					db $01,$FF,$01,$FF,$01,$00,$01,$FF		;bank 3 sizes

SUB_OFF_SCREEN_X1:   LDA #$02                ; \ entry point of routine determines value of $03
                    BRA STORE_03            ;  | (table entry to use on horizontal levels)
SUB_OFF_SCREEN_X2:   LDA #$04                ;  | 
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X3:   LDA #$06                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X4:   LDA #$08                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X5:   LDA #$0A                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X6:   LDA #$0C                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X7:   LDA #$0E                ;  |
STORE_03:			STA $03					;  |            
					BRA START_SUB			;  |
SUB_OFF_SCREEN_X0:   STZ $03					; /

START_SUB:           JSR SUB_IS_OFF_SCREEN   ; \ if sprite is not off screen, return
                    BEQ RETURN_35           ; /
                    LDA $5B                 ; \  goto VERTICAL_LEVEL if vertical level
                    AND #$01                ; |
                    BNE VERTICAL_LEVEL      ; /     
                    LDA $D8,x               ; \
                    CLC                     ; | 
                    ADC #$50                ; | if the sprite has gone off the bottom of the level...
                    LDA $14D4,x             ; | (if adding 0x50 to the sprite y position would make the high byte >= 2)
                    ADC #$00                ; | 
                    CMP #$02                ; | 
                    BPL ERASE_SPRITE        ; /    ...erase the sprite
                    LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0756 VC:176 00 FL:205
                    AND #$01                ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0780 VC:176 00 FL:205
                    ORA $03                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0796 VC:176 00 FL:205
                    STA $01                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0820 VC:176 00 FL:205
                    TAY                     ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0844 VC:176 00 FL:205
                    LDA $1A                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0858 VC:176 00 FL:205
                    CLC                     ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0882 VC:176 00 FL:205
                    ADC SPR_T14,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0896 VC:176 00 FL:205
                    ROL $00                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizcHC:0928 VC:176 00 FL:205
                    CMP $E4,x               ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:0966 VC:176 00 FL:205
                    PHP                     ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:0996 VC:176 00 FL:205
                    LDA $1B                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizCHC:1018 VC:176 00 FL:205
                    LSR $00                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdiZCHC:1042 VC:176 00 FL:205
                    ADC SPR_T15,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizcHC:1080 VC:176 00 FL:205
                    PLP                     ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F0 P:eNvMXdizcHC:1112 VC:176 00 FL:205
                    SBC $14E0,x             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1140 VC:176 00 FL:205
                    STA $00                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1172 VC:176 00 FL:205
                    LSR $01                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1196 VC:176 00 FL:205
                    BCC SPR_L31             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1234 VC:176 00 FL:205
                    EOR #$80                ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1250 VC:176 00 FL:205
                    STA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1266 VC:176 00 FL:205
SPR_L31:             LDA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1290 VC:176 00 FL:205
                    BPL RETURN_35           ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1314 VC:176 00 FL:205
ERASE_SPRITE:        LDA $14C8,x             ; \ if sprite status < 8, permanently erase sprite
                    CMP #$08                ; |
                    BCC KILL_SPRITE         ; /    
                    LDY $161A,x             ;A:FF08 X:0007 Y:0001 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1108 VC:059 00 FL:2878
                    CPY #$FF                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1140 VC:059 00 FL:2878
                    BEQ KILL_SPRITE         ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1156 VC:059 00 FL:2878
                    LDA #$00                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1172 VC:059 00 FL:2878
                    STA $1938,y             ;A:FF00 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZcHC:1188 VC:059 00 FL:2878
KILL_SPRITE:         STZ $14C8,x             ; erase sprite
RETURN_35:           RTS                     ; return

VERTICAL_LEVEL:      LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ; \
                    LSR A                   ; | 
                    BCS RETURN_35           ; /
                    LDA $E4,x               ; \ 
                    CMP #$00                ;  | if the sprite has gone off the side of the level...
                    LDA $14E0,x             ;  |
                    SBC #$00                ;  |
                    CMP #$02                ;  |
                    BCS ERASE_SPRITE        ; /  ...erase the sprite
                    LDA $13                 ;A:0000 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:1218 VC:250 00 FL:5379
                    LSR A                   ;A:0016 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1242 VC:250 00 FL:5379
                    AND #$01                ;A:000B X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1256 VC:250 00 FL:5379
                    STA $01                 ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1272 VC:250 00 FL:5379
                    TAY                     ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1296 VC:250 00 FL:5379
                    LDA $1C                 ;A:001A X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0052 VC:251 00 FL:5379
                    CLC                     ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0076 VC:251 00 FL:5379
                    ADC SPR_T12,y           ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0090 VC:251 00 FL:5379
                    ROL $00                 ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:enVMXdizCHC:0122 VC:251 00 FL:5379
                    CMP $D8,x               ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0160 VC:251 00 FL:5379
                    PHP                     ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0190 VC:251 00 FL:5379
                    LDA.w $001D             ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F2 P:eNVMXdizcHC:0212 VC:251 00 FL:5379
                    LSR $00                 ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdiZcHC:0244 VC:251 00 FL:5379
                    ADC SPR_T13,y           ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdizCHC:0282 VC:251 00 FL:5379
                    PLP                     ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:envMXdiZCHC:0314 VC:251 00 FL:5379
                    SBC $14D4,x             ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0342 VC:251 00 FL:5379
                    STA $00                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0374 VC:251 00 FL:5379
                    LDY $01                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0398 VC:251 00 FL:5379
                    BEQ SPR_L38             ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0422 VC:251 00 FL:5379
                    EOR #$80                ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0438 VC:251 00 FL:5379
                    STA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0454 VC:251 00 FL:5379
SPR_L38:             LDA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0478 VC:251 00 FL:5379
                    BPL RETURN_35           ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0502 VC:251 00 FL:5379
                    BMI ERASE_SPRITE        ;A:8AFF X:0002 Y:0000 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0704 VC:184 00 FL:5490

SUB_IS_OFF_SCREEN:   LDA $15A0,x             ; \ if sprite is on screen, accumulator = 0 
                    ORA $186C,x             ; |  
                    RTS                     ; / return

;SUBHORZPOS
;==========

SUB_HORZ_POS:        LDY #$00                ;A:25D0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1020 VC:097 00 FL:31642
                    SEP #$20
                    LDA $94                 ;A:25D0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1036 VC:097 00 FL:31642
                    SEC                     ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1060 VC:097 00 FL:31642
                    SBC $E4,x               ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1074 VC:097 00 FL:31642
                    STA $0E                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1104 VC:097 00 FL:31642
                    LDA $95                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1128 VC:097 00 FL:31642
                    SBC $14E0,x             ;A:2500 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZcHC:1152 VC:097 00 FL:31642
                    BPL LABEL16             ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1184 VC:097 00 FL:31642
                    INY                     ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1200 VC:097 00 FL:31642
LABEL16:             RTS                     ;A:25FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:1214 VC:097 00 FL:31642