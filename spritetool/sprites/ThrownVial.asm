;NOTE: The tile/property bytes for the "last-ditch effort" vial is not included in this table; it is coded directly into the graphics routine.
incsrc subroutinedefs_xkas.asm

Tilemap:            db $86,$A6,$A6,$86
PROPERTIES:         db $21,$21,$61,$61		;Note: This MUST exclude the palette part of the byte!  These just contain priority and X/Y flip, as well as graphics page.

FreeRAM:		;FreeRAM to use for the tilemap if the KooPhD's bit is set.  It must be a 24-bit address!
db $7F,$88,$09

!FreeRAM2 = $7F880D	;FreeRAM to use for the last-ditch potion.  It must be FreeRAM + 4, so in this case it would be $7F880D.


Vibrate:
db $00,$01,$00,$FF

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc

LDA #$40
STA $1510,X

LDA $D8,x
STA $00
LDA $14D4,x
STA $01
REP #$20
LDA $00
SEC
SBC #$0010
STA $00
SEP #$20
LDA $00
STA $D8,x
LDA $01
STA $14D4,x
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR ThrownVialMain
PLB
RTL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
DoNothing:
JMP Graphics

SuicideAttack:

STZ $B6,x
;LDA $1588,x
;AND #$04
;CMP #$04
;BNE DoNothing2

;DoNothing2:
;JSL $01802A

LDA $1510,X
CMP #$20
BCS SkipVibrate
LDA $13
AND #$03
PHX
TAX
LDA Vibrate,X
PLX
STA $157C,X
SkipVibrate:
LDA $1510,X

BNE DontExplode
LDA #$06
STA $1528,x
LDA #$40
STA $1510,X
Explode: 
JMP ExplodeLong
DontExplode:

JMP Graphics









ThrownVialMain:
LDA $9D
BEQ SpritesArentLockedNothingToSeeHere
JMP Graphics
SpritesArentLockedNothingToSeeHere:
JSR SUB_OFF_SCREEN_X0		; Handle offscreen.
LDA $1FD6,x
BNE DontUpdatePosition
JSL $01802A
DontUpdatePosition:
LDA $1588,X
AND #$04
CMP #$04
BEQ ContinueThing
LDA $1588,X
AND #$02
CMP #$02
BEQ ContinueThing
LDA $1588,X
AND #$01
CMP #$01
BEQ ContinueThing
LDA $1588,X
AND #$08
CMP #$08
BEQ ContinueThing

JMP Graphics

ContinueThing:
DEC $1510,x

LDA $1528,X	;Misc. sprite table used to index palette.
CMP #$05
BEQ SuicideAttack
CMP #$02
BEQ SpitFire
CMP #$04
BEQ Mushroom
CMP #$06
BEQ Explode


SpitFire:
LDA #$00
STA $00
JSR Fireball
LDA #$10
STA $00
JSR Fireball
LDA #$F0
STA $00
JSR Fireball

JSR ShatterVial

JSL $01ACF9	;Random sound effect for fireball
LDA $148D
EOR $148E
CLC
ADC $80
CLC
ADC $7E
AND #$07
CLC
ADC #$2D
STA $1DFC

STZ $14C8,x
JMP Graphics



Mushroom:

JSL $02A9DE
BMI Return

LDA $E4,x
STA $0C
LDA $D8,x
STA $0D
LDA $14E0,X
STA $0E
LDA $14D4,x
STA $0F

PHX
TYX
LDA #$01
STA $14C8,x
LDA #$74
STA $9E,x
JSL $07F7D2

;Store it to Mario's position.
LDA $0C
STA $E4,x
LDA $0E
STA $14E0,x
LDA $0D
STA $D8,x
LDA $0F
STA $14D4,x
LDA #$F0
STA $AA,x
PLX
STZ $14C8,x

JSR ShatterVial
LDA #$02
STA $1DFC

Return:
JMP Graphics



ExplodeLong:
LDA $1FD6,x
BNE DontPlaySoundEffect
LDA #$09
STA $1DFC
STA $1FD6,x
DontPlaySoundEffect:


LDA $1510,X
;BNE DontVanishSprite
;STZ $14C8,x
;DontVanishSprite:
STZ $B6,x
;LDA #$FF
STA $1540,x
LDA #$11
STA $1662,x

PHB
LDA #$02                ;  | 
PHA                       ;  | 
PLB                       ;  | 
JSL $028086       	;  | 
PLB                       ;  |
JSL $01A7DC
RTS







;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; bone-throwing subroutine, ripped from $03C44E
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Fireball:

LDA $15A0,x	; if the sprite is offscreen horizontally...
ORA $186C,x	; or vertically...
BNE NoSpawn	; don't throw a fireball

LDY #$07	; 8 extended sprite slots to loop through

ExSprSlotLoop:	;

LDA $170B,y	; if the slot is free...
BEQ SpawnBone	; then spawn a fireball sprite
DEY		; if not...
BPL ExSprSlotLoop	; decrement the index and try the next slot

NoSpawn:		;
RTS		;

SpawnBone:	;

LDA #$0B	; extended sprite number
STA $170B,y	; 06 = bone

LDA $D8,x	; sprite Y position

STA $1715,y	;
LDA $14D4,x	;
SBC #$00	; handle overflow
STA $1729,y	;

LDA $E4,x	; sprite X position
STA $171F,y	; no X offset at all
LDA $14E0,x	;
STA $1733,y	;

LDA $00
STA $1747,y	; extended sprite X speed table
LDA #$D0
STA $173D,y
RTS		;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

QuitGraphics2:
JMP QuitGraphics


Graphics:
LDA $1540,x
BNE QuitGraphics2

		
                    JSL !GetDrawInfo      ;A:87BF X:0007 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1102 VC:066 00 FL:665

	LDA $7FAB10,x
	AND #$04
	BNE Dynamic

	LDA.b #Tilemap
	STA $0B
	LDA.b #Tilemap>>8
	STA $0C
	LDA.b #Tilemap>>16
	STA $0D
	BRA DoneWithDrawingType
Dynamic:
	LDA FreeRAM
	STA $0D
	LDA FreeRAM+1
	STA $0C
	LDA FreeRAM+2
	STA $0B

DoneWithDrawingType:


		LDA $1528,x
		CLC				;ASDF CARRY YOU ARE THE BANE OF MY EXISTANCE POPPING UP IN PLACES WHERE I LEAST EXPECT YOU (read: before I added CLC carry was causing graphical glitchiness)
		ROL A
		STA $0E

LDA $157C,X
STA $04
		    LDA $14
		    AND #$03
                    PHX                     ; /
                    TAX

                    PHY                     ; set tile to be 16x16
                    TYA                     ;
                    LSR A                   ;
                    LSR A                   ;
                    TAY                     ;
                    LDA #$02	            ; 
                    STA $0460,y             ;
                    PLY                     ;
                    
                    LDA $00                 ; \ tile x position = sprite x location ($00) + tile displacement
			CLC
			ADC $04
                    STA $0300,y             ; /
                    
                    LDA $01                 ; |
                    STA $0301,y             ; /

			LDA $0E
			CMP #$0A
			BEQ DoSpecialTileMap
			PHY
			TXY
                    LDA [$0B],y           ; \ store tile
			PLY
                    STA $0302,y             ; /
			BRA DontDoSpecialTileMap
			DoSpecialTileMap:
			
			STX $00
			PLX
			LDA $7FAB10,x
			PHX
			LDX $00
			AND #$04
			BNE Dynamic2
			
			LDA #$82			;TILE FOR THE "FINAL ATTACK" VIAL
			STA $0302,y
			BRA DontDoSpecialTileMap
			Dynamic2:
			LDA !FreeRAM2
			STA $0302,y




			DontDoSpecialTileMap:

			LDA $0E
			CMP #$0A
			BEQ DoSpecialProperty
                    LDA PROPERTIES,x          ; flip tile if necessary
			BRA DontDoSpecialProperty
			DoSpecialProperty:
			LDA #$21			;PROPERTY FOR THE "FINAL ATTACK" VIAL
			DontDoSpecialProperty:


                    ORA $64                 ; add in tile priority of level
		    ORA $0E
                    STA $0303,y             ; store tile properties

                    PLX                     ; pull, X = sprite index
                    LDY #$FF                ; \ we've already set 460 so use FF
                    LDA #$00                ; | A = number of tiles drawn - 1
                    JSL $01B7B3             ; / don't draw if offscreen
QuitGraphics:       RTS                     ; return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Shatter Vial Routine.  This is ripped directly from the Yoshi's Egg sprite.  Yay for no documentation! =D
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ShatterVial:

CODE_01F7C8:	JSR.w IsSprOffScreen      
CODE_01F7CB:	BNE Return01F82C          
CODE_01F7CD:	LDA $E4,x
CODE_01F7CF:	STA $00                   
CODE_01F7D1:	LDA $D8,x   
CODE_01F7D3:	STA $02                   
CODE_01F7D5:	LDA.w $14D4,x
CODE_01F7D8:	STA $03                   
CODE_01F7DA:	PHX                       
CODE_01F7DB:	LDY.b #$03                
CODE_01F7DD:	LDX.b #$0B                
CODE_01F7DF:	LDA.w $17F0,X             
CODE_01F7E2:	BEQ CODE_01F7F4           
CODE_01F7E4:	DEX                       
CODE_01F7E5:	BPL CODE_01F7DF           
CODE_01F7E7:	DEC.w $185D               
CODE_01F7EA:	BPL CODE_01F7F1           
CODE_01F7EC:	LDA.b #$0B                
CODE_01F7EE:	STA.w $185D               
CODE_01F7F1:	LDX.w $185D               
CODE_01F7F4:	LDA.b #$03                
CODE_01F7F6:	STA.w $17F0,X             
CODE_01F7F9:	LDA $00                   
CODE_01F7FB:	CLC                       
CODE_01F7FC:	ADC.w DATA_01F831,Y       
CODE_01F7FF:	STA.w $1808,X             
CODE_01F802:	LDA $02                   
CODE_01F804:	CLC                       
CODE_01F805:	ADC.w DATA_01F82D,Y       
CODE_01F808:	STA.w $17FC,X             
CODE_01F80B:	LDA $03                   
CODE_01F80D:	STA.w $1814,X             
CODE_01F810:	LDA.w DATA_01F835,Y       
CODE_01F813:	STA.w $1820,X             
CODE_01F816:	LDA.w DATA_01F839,Y       
CODE_01F819:	STA.w $182C,X             
CODE_01F81C:	TYA                       
CODE_01F81D:	ASL                       
CODE_01F81E:	ASL                       
CODE_01F81F:	ASL                       
CODE_01F820:	ASL                       
CODE_01F821:	ASL                       
CODE_01F822:	ASL                       
CODE_01F823:	ORA.b #$28                
CODE_01F825:	STA.w $1850,X             
CODE_01F828:	DEY                       
CODE_01F829:	BPL CODE_01F7E4           
CODE_01F82B:	PLX                       
Return01F82C:	RTS                       ; Return 


IsSprOffScreen:	LDA.w $15A0,X ; \ A = Current sprite is offscreen 
CODE_0180CE:	ORA.w $186C,X ; /  
Return0180D1:	RTS                       ; Return 



DATA_01F82D:                      db $00,$00,$08,$08

DATA_01F831:                      db $00,$08,$00,$08

DATA_01F835:                      db $E8,$E8,$F4,$F4

DATA_01F839:                      db $FA,$06,$FD,$03



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; routines below can be shared by all sprites.  they are ripped from original
; SMW and poorly documented
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; $B817 - horizontal mario/sprite check - shared
; Y = 1 if mario left of sprite??
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    ;org $03B817                ; Y = 1 if contact

SUB_GET_DIR:         LDY #$00                ;A:25D0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1020 VC:097 00 FL:31642
                    LDA $94                 ;A:25D0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1036 VC:097 00 FL:31642
                    SEC                     ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1060 VC:097 00 FL:31642
                    SBC $E4,x               ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1074 VC:097 00 FL:31642
                    STA $0F                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1104 VC:097 00 FL:31642
                    LDA $95                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1128 VC:097 00 FL:31642
                    SBC $14E0,x             ;A:2500 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZcHC:1152 VC:097 00 FL:31642
                    BPL LABEL16             ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1184 VC:097 00 FL:31642
                    INY                     ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1200 VC:097 00 FL:31642
LABEL16:             RTS                     ;A:25FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:1214 VC:097 00 FL:31642

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; $B829 - vertical mario/sprite position check - shared
; Y = 1 if mario below sprite??
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    ;org $03B829 

SUB_VERT_POS:        LDY #$00               ;A:25A1 X:0007 Y:0001 D:0000 DB:03 S:01EA P:envMXdizCHC:0130 VC:085 00 FL:924
                    LDA $96                ;A:25A1 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdiZCHC:0146 VC:085 00 FL:924
                    SEC                    ;A:2546 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdizCHC:0170 VC:085 00 FL:924
                    SBC $D8,x              ;A:2546 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdizCHC:0184 VC:085 00 FL:924
                    STA $0F                ;A:25D6 X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0214 VC:085 00 FL:924
                    LDA $97                ;A:25D6 X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0238 VC:085 00 FL:924
                    SBC $14D4,x            ;A:2501 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdizcHC:0262 VC:085 00 FL:924
                    BPL LABEL11            ;A:25FF X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0294 VC:085 00 FL:924
                    INY                    ;A:25FF X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0310 VC:085 00 FL:924
LABEL11:             RTS                    ;A:25FF X:0007 Y:0001 D:0000 DB:03 S:01EA P:envMXdizcHC:0324 VC:085 00 FL:924


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; $B817 - horizontal mario/sprite check - shared
; Y = 1 if mario left of sprite??
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    ;org $03B817        

SUB_HORZ_POS:        LDY #$00                ;A:25D0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1020 VC:097 00 FL:31642
                    LDA $94                 ;A:25D0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1036 VC:097 00 FL:31642
                    SEC                     ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1060 VC:097 00 FL:31642
                    SBC $E4,x               ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1074 VC:097 00 FL:31642
                    STA $0F                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1104 VC:097 00 FL:31642
                    LDA $95                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1128 VC:097 00 FL:31642
                    SBC $14E0,x             ;A:2500 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZcHC:1152 VC:097 00 FL:31642
                    BPL LABEL17             ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1184 VC:097 00 FL:31642
                    INY                     ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1200 VC:097 00 FL:31642
LABEL17:             RTS                     ;A:25FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:1214 VC:097 00 FL:31642


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_OFF_SCREEN
; This subroutine deals with sprites that have moved off screen
; It is adapted from the subroutine at $01AC0D
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
SPR_T12:             db $40,$B0
SPR_T13:             db $01,$FF
SPR_T14:             db $30,$C0,$A0,$C0,$A0,$F0,$60,$90		;bank 1 sizes
		            db $30,$C0,$A0,$80,$A0,$40,$60,$B0		;bank 3 sizes
SPR_T15:             db $01,$FF,$01,$FF,$01,$FF,$01,$FF		;bank 1 sizes
					db $01,$FF,$01,$FF,$01,$00,$01,$FF		;bank 3 sizes

SUB_OFF_SCREEN_X1:   LDA #$02                ; \ entry point of routine determines value of $03
                    BRA STORE_03            ;  | (table entry to use on horizontal levels)
SUB_OFF_SCREEN_X2:   LDA #$04                ;  | 
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X3:   LDA #$06                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X4:   LDA #$08                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X5:   LDA #$0A                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X6:   LDA #$0C                ;  |
                    BRA STORE_03            ;  | OMG YOU FOUND THIS HIDDEN z0mg place!111 you win a cookie!
SUB_OFF_SCREEN_X7:   LDA #$0E                ;  |
STORE_03:			STA $03					;  |            
					BRA START_SUB			;  |
SUB_OFF_SCREEN_X0:   STZ $03					; /

START_SUB:           JSR SUB_IS_OFF_SCREEN   ; \ if sprite is not off screen, return
                    BEQ RETURN_35           ; /
                    LDA $5B                 ; \  goto VERTICAL_LEVEL if vertical level
                    AND #$01                ; |
                    BNE VERTICAL_LEVEL      ; /     
                    LDA $D8,x               ; \
                    CLC                     ; | 
                    ADC #$50                ; | if the sprite has gone off the bottom of the level...
                    LDA $14D4,x             ; | (if adding 0x50 to the sprite y position would make the high byte >= 2)
                    ADC #$00                ; | 
                    CMP #$02                ; | 
                    BPL ERASE_SPRITE        ; /    ...erase the sprite
                    LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0756 VC:176 00 FL:205
                    AND #$01                ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0780 VC:176 00 FL:205
                    ORA $03                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0796 VC:176 00 FL:205
                    STA $01                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0820 VC:176 00 FL:205
                    TAY                     ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0844 VC:176 00 FL:205
                    LDA $1A                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0858 VC:176 00 FL:205
                    CLC                     ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0882 VC:176 00 FL:205
                    ADC SPR_T14,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0896 VC:176 00 FL:205
                    ROL $00                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizcHC:0928 VC:176 00 FL:205
                    CMP $E4,x               ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:0966 VC:176 00 FL:205
                    PHP                     ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:0996 VC:176 00 FL:205
                    LDA $1B                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizCHC:1018 VC:176 00 FL:205
                    LSR $00                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdiZCHC:1042 VC:176 00 FL:205
                    ADC SPR_T15,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizcHC:1080 VC:176 00 FL:205
                    PLP                     ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F0 P:eNvMXdizcHC:1112 VC:176 00 FL:205
                    SBC $14E0,x             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1140 VC:176 00 FL:205
                    STA $00                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1172 VC:176 00 FL:205
                    LSR $01                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1196 VC:176 00 FL:205
                    BCC SPR_L31             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1234 VC:176 00 FL:205
                    EOR #$80                ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1250 VC:176 00 FL:205
                    STA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1266 VC:176 00 FL:205
SPR_L31:             LDA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1290 VC:176 00 FL:205
                    BPL RETURN_35           ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1314 VC:176 00 FL:205
ERASE_SPRITE:        LDA $14C8,x             ; \ if sprite status < 8, permanently erase sprite
                    CMP #$08                ; |
                    BCC KILL_SPRITE         ; /    
                    LDY $161A,x             ;A:FF08 X:0007 Y:0001 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1108 VC:059 00 FL:2878
                    CPY #$FF                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1140 VC:059 00 FL:2878
                    BEQ KILL_SPRITE         ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1156 VC:059 00 FL:2878
                    LDA #$00                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1172 VC:059 00 FL:2878
                    STA $1938,y             ;A:FF00 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZcHC:1188 VC:059 00 FL:2878
KILL_SPRITE:         STZ $14C8,x             ; erase sprite
RETURN_35:           RTS                     ; return

VERTICAL_LEVEL:      LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ; \
                    LSR A                   ; | 
                    BCS RETURN_35           ; /
                    LDA $E4,x               ; \ 
                    CMP #$00                ;  | if the sprite has gone off the side of the level...
                    LDA $14E0,x             ;  |
                    SBC #$00                ;  |
                    CMP #$02                ;  |
                    BCS ERASE_SPRITE        ; /  ...erase the sprite
                    LDA $13                 ;A:0000 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:1218 VC:250 00 FL:5379
                    LSR A                   ;A:0016 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1242 VC:250 00 FL:5379
                    AND #$01                ;A:000B X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1256 VC:250 00 FL:5379
                    STA $01                 ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1272 VC:250 00 FL:5379
                    TAY                     ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1296 VC:250 00 FL:5379
                    LDA $1C                 ;A:001A X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0052 VC:251 00 FL:5379
                    CLC                     ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0076 VC:251 00 FL:5379
                    ADC SPR_T12,y           ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0090 VC:251 00 FL:5379
                    ROL $00                 ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:enVMXdizCHC:0122 VC:251 00 FL:5379
                    CMP $D8,x               ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0160 VC:251 00 FL:5379
                    PHP                     ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0190 VC:251 00 FL:5379
                    LDA $001D             ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F2 P:eNVMXdizcHC:0212 VC:251 00 FL:5379
                    LSR $00                 ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdiZcHC:0244 VC:251 00 FL:5379
                    ADC SPR_T13,y           ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdizCHC:0282 VC:251 00 FL:5379
                    PLP                     ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:envMXdiZCHC:0314 VC:251 00 FL:5379
                    SBC $14D4,x             ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0342 VC:251 00 FL:5379
                    STA $00                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0374 VC:251 00 FL:5379
                    LDY $01                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0398 VC:251 00 FL:5379
                    BEQ SPR_L38             ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0422 VC:251 00 FL:5379
                    EOR #$80                ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0438 VC:251 00 FL:5379
                    STA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0454 VC:251 00 FL:5379
SPR_L38:             LDA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0478 VC:251 00 FL:5379
                    BPL RETURN_35           ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0502 VC:251 00 FL:5379
                    BMI ERASE_SPRITE        ;A:8AFF X:0002 Y:0000 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0704 VC:184 00 FL:5490

SUB_IS_OFF_SCREEN:   LDA $15A0,x             ; \ if sprite is on screen, accumulator = 0 
                    ORA $186C,x             ; |  
                    RTS                     ; / return