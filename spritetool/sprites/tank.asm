!HP1	= $03	; hp in phase 1
!HP2	= $10	; how many seconds before helicopter crashes.

incsrc subroutinedefs_xkas.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	PRINT "INIT ",pc
	LDA #$FC
	STA $14E0,x
	LDA #$70
	STA $B6,x
	LDA #$90
	STA $1540,x
	STZ $1594,x
	RTL

	PRINT "MAIN ",pc
	PHB
	PHK
	PLB
	JSR SPRITE_ROUTINE
	PLB
	RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
RETURN:	RTS
SPRITE_ROUTINE:
	LDA $1558,x
	AND #$01
	BNE NOFLASH
	JSR SUB_GFX
NOFLASH:	LDA $9D
	BNE RETURN
	JSL $01A7DC
	LDA $C2,x
	JSL $0086DF
	dw STATE0&$FFFF
	dw STATE1&$FFFF
	dw STATE2&$FFFF
	dw STATE3&$FFFF
	dw STATE4&$FFFF
STATE0:	LDA $1540,x
	CMP #$40
	BCC NOMOVLAYER1
	CMP #$80
	BCS RETURN0
	LDA $1540,x
	AND #$03
	BNE NOMOVLAYER1
	LDA $1462
	CLC
	ADC #$01
	STA $1462
	STA $1466
	LDA $1463
	ADC #$00
	STA $1463
	STA $1467
	RTS
NOMOVLAYER1:
	JSL $01802A
	LDA $14
	LSR
	LSR
	AND #$01
	TAY
	STA $1602,x
	STZ $AA,x
	LDA $B6,x
	BEQ INCSTT0
	LDA $14
	AND #$01
	BEQ RETURN0
	DEC $B6,x
RETURN0:	RTS
INCSTT0:	LDA #$01
	STA $C2,x
	LDA $E4,x
	STA $1510,x
	RTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
STATE1:	JSL $01802A
	LDY #$0C
LOOP:	CPY #$00
	BNE SPCON
	JML NOSPCON
SPCON:	DEY
	STX $06
	CPY $06
	BEQ LOOP
	JSL $03B69F
	PHX
	TYX
	JSL $03B6E5
	PLX
	JSL $03B72B
	BCC LOOP
	LDA $14C8,y
	CMP #$08
	BNE LOOP
	LDA $1510,y
	BEQ KILLSP
	LDA $00B6,y
	BMI HIT
	CMP #$08
	BCS KILLSP
HIT:	LDA #$13
	STA $1DF9
	INC $1594,x
	LDA $1594,x
	CMP #!HP1
	BNE SETTIMER1
	LDA #$02
	STA $C2,x
	STZ $1594,x
	LDA #$90
	STA $1540,x
	STZ $B6,x
	LDA #$E0
	STA $AA,x
	LDA $D8,x
	SEC
	SBC #$08
	STA $D8,x
	LDA $14D4,x
	SBC #$00
	STA $14D4,x
	LDA #$02
	STA $1602,x
	BRA KILLNORMAL
KILLSP:	LDA #$01
	STA $1DF9
	BRA KILLNORMAL
SETTIMER1:
	LDA #$20
	STA $1558,x
KILLNORMAL:
	LDA #$02
	STA $14C8,y
	LDA #$20
	STA $00B6,y
	LDA #$E0
	STA $00AA,y
NOSPCON:	STZ $AA,x
	LDA $14
	LSR
	LSR
	AND #$01
	TAY
	STA $1602,x
	LDA $1540,x
	BNE TIMERSET1
	JSL $01ACF9
	ADC $94
	AND #$FF
	BEQ SHOOTSPR
	LDA $163E,x
	BNE RETURN1
	JSL $01ACF9
	EOR $14
	ADC $7B
	AND #$7F
	BNE RETURN1
	LDA #$EC
	STA $1540,x
RETURN1:	RTS
TIMERSET1:
	CMP #$D0
	BEQ SHOT1
	BCC DECSPD1
	LDA #$F0
	STA $B6,x
	RTS
SHOT1:	LDA #$60
	STA $B6,x
	LDA #$17
	STA $1DFC
DECSPD1:	DEC $B6,x
	LDA $1540,x
	CMP #$16
	BCS RETURN1
	LDA $1510,x
	STA $E4,x
	STZ $15A0,x
	STZ $B6,x
	STZ $1540,x
	RTS
SHOOTSPR:
	LDA #$20
	STA $163E,x
	LDA #$09
	STA $1DFC
	JSR SMOKE
	LDA $D8,x
	SEC
	SBC #$20
	STA $17C4,y
	LDA $E4,x
	CLC
	ADC #$20
	STA $17C8,y
	JSL SETSPRITE
	LDA $E4,x
	CLC
	ADC #$1A
	STA $00E4,y
	LDA $14E0,x
	ADC #$00
	STA $14E0,y
	LDA $D8,x
	SEC
	SBC #$1A
	STA $00D8,y
	LDA $14D4,x
	SBC #$00
	STA $14D4,y
	JSL SETSPR2
	LDA #$C0
	STA $00AA,y
	LDA #$40
	STA $00B6,y
	PHX
	JSL $01ACF9
	AND #$01
	TAX
	STA $1510,y
	PLX
	LDA #$90
	STA $1540,y
	RTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
STATE2:	JSL $01802A
	LDA $1594,x
	JSL $0086DF
	dw STATE2_0&$FFFF
	dw STATE2_1&$FFFF
SPAWNY:	db $40,$50,$60
STATE2_0:
	LDA #$02
	STA $1602,x
	LDA $1588,x
	AND #$04
	BEQ NOGND2_2
	LDA $AA,x
	CMP #$08
	BCS NOGND2
	STZ $AA,x
	BRA NOGND2_2
NOGND2:	LDA $AA,x
	LSR A
	EOR #$FF
	INC A
	STA $AA,x
	LDA #$01
	STA $1DF9
NOGND2_2:
	LDA $B6,x
	BEQ DECSPD
	CMP #$D0
	BCC NOTMINUS
DECSPD:	LDA $14
	AND #$03
	BNE NOTMINUS
	DEC $B6,x
NOTMINUS:
	LDA $14E0,x
	BPL RETURN2
	LDA $E4,x
	CMP #$A0
	BCS RETURN2
	LDA #$01
	STA $1594,x
	LDA #$08
	STA $D8,x
	LDA #$01
	STA $14D4,x
	LDA #$B8
	STA $1540,x
RETURN2:	RTS
STATE2_1:
	JSR SHOWFRM
	LDA $1540,x
	BNE TIMERSET2
	LDA $14
	AND #$01
	BEQ RETURN2
	LDA $B6,x
	BEQ NOSPD2
	DEC $B6,x
	RTS
TIMERSET2:
	LDA #$18
	STA $B6,x
	RTS
NOSPD2:	STZ $1594,x
	LDA #$03
	STA $C2,x
	RTS
XSPD:	db $F8,$FE
YSPD:	db $02,$04,$08,$02,$FE,$FC,$F8,$FE
FLYFRM:	db $03,$04,$05,$04
SHOWFRM:	LDA $14
	LSR A
	LSR A
	LSR A
	AND #$07
	TAY
	LDA YSPD,y
	STA $AA,x
	LDA $14
	LSR : LSR
	AND #$03
	TAY
	LDA FLYFRM,y
	STA $1602,x
	RTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
STATE3:	JSL $01802A
	JSR SHOWFRM
	LDA $14
	AND #$3F
	BNE NOSECOND
	INC $1594,x
	LDA $1594,x
	CMP #!HP2
	BCC NOSECOND
	LDA #$04
	STA $C2,x
	STZ $1594,x
NOSECOND:
	LDA $14
	LSR A
	LSR A
	LSR A
	AND #$07
	TAY
	LDA XSPD,y
	STA $B6,x
	LDA $1540,x
	BNE RETURN3
	JSL $01ACF9
	ADC $14
	SBC $96
	EOR $94
	AND #$3F
	BNE RETURN3
	LDA #$10
	STA $1540,x
	JSR SMOKE
	LDA $D8,x
	CLC
	ADC #$1A
	STA $17C4,y
	LDA $E4,x
	CLC
	ADC #$08
	STA $17C8,y
	JSL SETSPRITE
	LDA $D8,x
	CLC
	ADC #$1A
	STA $00D8,y
	LDA $14D4,x
	ADC #$00
	STA $14D4,y
	LDA $E4,x
	CLC
	ADC #$0C
	STA $00E4,y
	LDA $14E0,x
	ADC #$00
	STA $14E0,y
	JSL SETSPR2
	JSL $01ACF9
	AND #$03
	BNE BALL
	LDA #$03
	BRA BALL2
BALL:	LDA #$02
BALL2:	STA $1510,y
	LDA #$58
	STA $1540,x
	LDA #$28
	STA $00B6,y
	LDA #$23
	STA $1DF9
RETURN3:	RTS

STATE4:	JSL $01802A
	LDA $1594,x
	BNE SKIP4
	LDA $14
	LSR : LSR
	AND #$03
	TAY
	LDA FLYFRM,y
	STA $1602,x

	LDA $14
	AND #$01
	BNE MAXSPD
	INC $B6,x
MAXSPD:	STZ $AA,x
	LDA $1588,x
	AND #$03
	BEQ RETURN4
	LDA #$02
	STA $1602,x
	INC $1594,x
	LDA #$A0
	STA $1540,x
	LDA #$09
	STA $1DFC
	LDA #$E0
	STA $AA,x
	LDA #$E0
	STA $B6,x
RETURN4:	RTS
SKIP4:	LDA $1540,x
	BEQ TIMERDONE
	LDA $7FC210
	CMP #$06
	BCC NOSPD
	LDA $7FC210
	DEC A
	DEC A
	STA $7FC210
	BRA DECSPD4
NOSPD:	LDA #$06
	STA $7FC210
	LDA $1462
	CLC
	ADC #$01
	STA $1462
	LDA $1463
	ADC #$00
	STA $1463

	LDA $94
	CLC
	ADC #$01
	STA $94
	LDA $95
	ADC #$00
	STA $95
	BRA DECSPD4
TIMERDONE:
	LDA #$00
	STA $7FC210
DECSPD4:	LDA $1564,x
	BEQ CHKGND4
	CMP #$01
	BNE NOGND4_2
	LDA #$0B
	STA $71
	LDA #$FF
	STA $1493
	INC $9D
	RTS
CHKGND4:	LDA $1588,x
	AND #$04
	BEQ NOGND4_2
	LDA $AA,x
	ORA $B6,x
	BEQ NOGND4_2
	STZ $B6,x
	STZ $AA,x
	LDA #$60
	STA $1564,x
	LDA #$10
	STA $1DFC
NOGND4_2:
	RTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SETSPRITE:
	JSL $02A9DE
	BMI NOSPR
	LDA #$01
	STA $14C8,y
	LDA #$B3
	PHX
	TYX
	STA $7FAB9E,x
	PLX
NOSPR:	RTL
SETSPR2:	PHX
	TYX
	JSL $07F7D2
	JSL $0187A7
	LDA #$08
	STA $7FAB10,x
	PLX
	RTL
SMOKE:	LDY #$03
FIND_FREE:
	LDA $17C0,y
	BEQ FOUND_ONE
	DEY
	BPL FIND_FREE
	RTS
FOUND_ONE:
	LDA #$01
	STA $17C0,y
	LDA #$14
	STA $17CC,y
	RTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
TILEMAP:		db $80,$82,$A0,$A2,$8C
		db $A4,$A6,$A8,$A8,$A4

		db $80,$82,$A0,$A2,$8C
		db $A4,$A6,$AA,$AA,$A4

		db $80,$82,$A0,$A2,$80
		db $80,$82,$A0,$A2,$80

		db $80,$82,$A0,$A2,$8E
		db $84,$86,$8A,$8A,$8A


		db $80,$82,$A0,$A2,$8E
		db $84,$86,$EA,$EA,$EA

		db $80,$82,$A0,$A2,$8E
		db $84,$86,$E8,$EA,$E8

X_OFFSET:	db $00,$10,$00,$10,$14
		db $00,$10,$00,$10,$00

		db $00,$10,$00,$10,$14
		db $00,$10,$00,$10,$00

		db $00,$10,$00,$10,$00
		db $00,$10,$00,$10,$00

		db $00,$10,$00,$10,$08
		db $00,$10,$08,$08,$08

		db $00,$10,$00,$10,$08
		db $00,$10,$08,$08,$08

		db $00,$10,$00,$10,$08
		db $00,$10,$F8,$08,$18

Y_OFFSET:	db $E9,$E9,$F6,$F6,$E5
		db $F6,$F6,$06,$06,$F6

		db $E8,$E8,$F5,$F5,$E4
		db $F5,$F5,$05,$05,$F5

		db $F1,$F1,$01,$01,$F1
		db $F1,$F1,$01,$01,$F1

		db $F5,$F5,$05,$05,$0F
		db $F7,$F7,$E7,$E7,$E7

		db $F5,$F5,$05,$05,$0F
		db $F7,$F7,$E7,$E7,$E7

		db $F5,$F5,$05,$05,$0F
		db $F7,$F7,$E7,$E7,$E7

PROPERTIES:	db $00,$00,$00,$00,$00
		db $00,$00,$00,$40,$00

		db $00,$00,$00,$00,$00
		db $00,$00,$00,$40,$00

		db $00,$00,$00,$00,$00
		db $00,$00,$00,$00,$00

		db $00,$00,$00,$00,$00
		db $00,$00,$00,$00,$00

		db $00,$00,$00,$00,$00
		db $00,$00,$00,$00,$00

		db $00,$00,$00,$00,$00
		db $00,$00,$00,$00,$00

SUB_GFX:
	JSL !GetDrawInfo       ; sets y = OAM offset

	LDA $1602,x
	ASL A
	ASL A
	ASL A
	ADC $1602,x
	ADC $1602,x
	STA $03

	LDA #$07
	CMP $1602,x
	BEQ NOGFX
	PHX
	LDX #$09
LOOP_START:          PHX
	TXA
	CLC
	ADC $03
	TAX

	LDA X_OFFSET,x
	CLC
	ADC $00                 ; \ tile x position = sprite y location ($01)
	STA $0300,y             ; /

	LDA Y_OFFSET,x
	CLC
	ADC $01                 ; \ tile y position = sprite x location ($00)
	STA $0301,y             ; /

	LDA TILEMAP,x
	STA $0302,y 

	PHX
	LDX $15E9
	LDA $15F6,x             ; tile PROPERTIES xyppccct, format
	PLX
	ORA PROPERTIES,x
	ORA $64                 ; add in tile priority of level
	STA $0303,y             ; store tile PROPERTIES
	PLX

	INY	 ; \ increase index to sprite tile map ($300)...
	INY	 ;  |    ...we wrote 1 16x16 tile...
	INY	 ;  |    ...sprite OAM is 8x8...
	INY	 ; /    ...so increment 4 times
	DEX
	BPL LOOP_START
	PLX

	LDY #$02                ; \ 460 = 2 (all 16x16 tiles)
	LDA #$09               ;  | A = (number of tiles drawn - 1)
	JSL $01B7B3             ; / don't draw if offscreen
NOGFX:	RTS	 ; RETURN
