;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
incsrc subroutinedefs_xkas.asm

!FrameNum = $C2

                    print "INIT ",pc
					STZ !FrameNum,x
					RTL
	                print "MAIN ",pc			
                    PHB
                    PHK				
                    PLB
                    JSR SPRITE_ROUTINE			;Jump to the routine to keep organized
                    PLB
                    RTL     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

XSpeeds:
db $10,$F0

SPRITE_ROUTINE:	  	
					LDA $1588,x
					BIT #$07
					BEQ ++
					LDA $1588,x
					BIT #$03
					BNE +
					STZ $B6,x
					BRA ++
					+
					LDA $B6,x
					EOR #$FF
					INC
					STA $B6,x
					++
					LDA $9D
					BNE return
					LDA $14C8,x
					CMP #$08
					BNE return
					STZ $00
					STZ !FrameNum,x
					JSR Proximity
					REP #$20
					PHA
					CMP #$0050
					BCS +
					PLA
					PHA
					SEP #$20
					LDA #$01
					STA !FrameNum,x
					STA $00
					REP #$20
					PLA
					PHA
					CMP #$0020
					BCS +
					PLA
					SEP #$20
					LDA #$02
					STA !FrameNum,x
					STA $00
					REP #$20
					PHA
					+
					PLA
					SEP #$20
					
					LDA $00
					CMP #$02
					BCC +
					LDA $1588,x
					BIT #$04
					BEQ +
					LDA #$E0
					STA $AA,x
					PHX
					JSR SUB_HORZ_POS
					TYX
					LDA XSpeeds,x
					PLX
					STA $B6,x
					+
					JSL $01802A
					JSL $01803A
					return:
					JSR SUB_OFF_SCREEN_X0
					JSR SUB_GFX					;Draw the graphics
                    RTS							;End the routine
					
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Proximity:
LDA $14E0,x
XBA
LDA $E4,x
REP #$20
SEC
SBC $D1
BPL +
EOR #$FFFF
INC
+
SEP #$20
RTS

SUB_HORZ_POS:		LDY #$00
					LDA $94
					SEC
					SBC $E4,x
					STA $0F
					LDA $95
					SBC $14E0,x
					BPL +
					INY
					+
					RTS

TileNums:
db $00,$00,$02
TileSize:
db $02,$00,$00

TilePointers:
dw Tile1
dw Tile2
dw Tiles3

XOff:
db $00,$00,$08

YOff:
db $00,$F8,$F8

Tile1:
db $A7
Tile2:
db $A9
Tiles3:
db $AB,$98,$99

SUB_GFX:            JSL !GetDrawInfo       	;Get all the drawing info, duh
					STZ $08
					LDA $14C8,x
					CMP #$08
					BEQ +
					LDA #$80
					STA $08
					LDA #$00
					STA !FrameNum,x
					+
					PHX
					LDA !FrameNum,x
					ASL
					TAX
					REP #$20
					LDA TilePointers,x
					STA $05
					SEP #$20
					TXA
					LSR
					TAX
					LDA TileNums,x
					STA $07
					TAX
					.GFXLoop
					
					LDA $00
					CLC
					ADC XOff,x
					STA $0300,y
					
					LDA $01
					CLC
					ADC YOff,x
					STA $0301,y
					
					PHY
					TXY
					LDA ($05),y
					PLY
					STA $0302,y
					PHX
					LDX $15E9
					LDA $15F6,x
					PLX
					ORA $64
					ORA $08
					STA $0303,y
					PHY
					TYA
					LSR #2
					TAY
					LDA TileSize,x
					PHX
					LDX $15E9
					ORA $15A0,x	; horizontal offscreen flag
					PLX
					STA $0460,y	;
					PLY
					INY
					INY
					INY
					INY
					DEX
					BPL .GFXLoop
					PLX
					LDA $07
					LDY #$FF
					JSL $01B7B3					;/and then draw em
					EndIt:
					RTS
					
					
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_OFF_SCREEN
; This subroutine deals with sprites that have moved off screen
; It is adapted from the subroutine at $01AC0D
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
SPR_T12:             db $40,$B0
SPR_T13:             db $01,$FF
SPR_T14:             db $30,$C0,$A0,$C0,$A0,$F0,$60,$90		;bank 1 sizes
		            db $30,$C0,$A0,$80,$A0,$40,$60,$B0		;bank 3 sizes
SPR_T15:             db $01,$FF,$01,$FF,$01,$FF,$01,$FF		;bank 1 sizes
					db $01,$FF,$01,$FF,$01,$00,$01,$FF		;bank 3 sizes

SUB_OFF_SCREEN_X1:   LDA #$02                ; \ entry point of routine determines value of $03
                    BRA STORE_03            ;  | (table entry to use on horizontal levels)
SUB_OFF_SCREEN_X2:   LDA #$04                ;  | 
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X3:   LDA #$06                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X4:   LDA #$08                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X5:   LDA #$0A                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X6:  LDA #$0C                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X7:   LDA #$0E                ;  |
STORE_03:			STA $03					;  |            
					BRA START_SUB			;  |
SUB_OFF_SCREEN_X0:   STZ $03					; /

START_SUB:           JSR SUB_IS_OFF_SCREEN   ; \ if sprite is not off screen, return
                    BEQ RETURN_35           ; /
                    LDA $5B                 ; \  goto VERTICAL_LEVEL if vertical level
                    AND #$01                ; |
                    BNE VERTICAL_LEVEL      ; /     
                    LDA $D8,x               ; \
                    CLC                     ; | 
                    ADC #$50                ; | if the sprite has gone off the bottom of the level...
                    LDA $14D4,x             ; | (if adding 0x50 to the sprite y position would make the high byte >= 2)
                    ADC #$00                ; | 
                    CMP #$02                ; | 
                    BPL ERASE_SPRITE        ; /    ...erase the sprite
                    LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13
                    AND #$01
                    ORA $03
                    STA $01
                    TAY
                    LDA $1A
                    CLC
                    ADC SPR_T14,y
                    ROL $00
                    CMP $E4,x
                    PHP
                    LDA $1B
                    LSR $00
                    ADC SPR_T15,y
                    PLP
                    SBC $14E0,x
                    STA $00
                    LSR $01
                    BCC SPR_L31
                    EOR #$80
                    STA $00
SPR_L31:             LDA $00
                    BPL RETURN_35
ERASE_SPRITE:        LDA $14C8,x             ; \ if sprite status < 8, permanently erase sprite
                    CMP #$08                ; |
                    BCC KILL_SPRITE         ; /    
                    LDY $161A,x
                    CPY #$FF
                    BEQ KILL_SPRITE
                    LDA #$00
                    STA $1938,y
KILL_SPRITE:         STZ $14C8,x             ; erase sprite
RETURN_35:           RTS                     ; return

VERTICAL_LEVEL:      LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ; \
                    LSR A                   ; | 
                    BCS RETURN_35           ; /
                    LDA $E4,x               ; \ 
                    CMP #$00                ;  | if the sprite has gone off the side of the level...
                    LDA $14E0,x             ;  |
                    SBC #$00                ;  |
                    CMP #$02                ;  |
                    BCS ERASE_SPRITE        ; /  ...erase the sprite
                    LDA $13
                    LSR A
                    AND #$01
                    STA $01
                    TAY
                    LDA $1C
                    CLC
                    ADC SPR_T12,y
                    ROL $00
                    CMP $D8,x
                    PHP
                    LDA.w $001D 
                    LSR $00
                    ADC SPR_T13,y
                    PLP
                    SBC $14D4,x
                    STA $00
                    LDY $01
                    BEQ SPR_L38
                    EOR #$80
                    STA $00
SPR_L38:             
					LDA $00
                    BPL RETURN_35
                    BMI ERASE_SPRITE

SUB_IS_OFF_SCREEN:   LDA $15A0,x             ; \ if sprite is on screen, accumulator = 0 
                    ORA $186C,x             ; |  
                    RTS                     ; / return

