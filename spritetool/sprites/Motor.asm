;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
incsrc subroutinedefs_xkas.asm

!FrameNum = $C2
!Direction = $157C

                    print "INIT ",pc
					STZ !FrameNum,x
					LDA #$01
					STA !Direction,x
					RTL
	                print "MAIN ",pc			
                    PHB
                    PHK				
                    PLB
                    JSR SPRITE_ROUTINE			;Jump to the routine to keep organized
                    PLB
                    RTL     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

XSpeeds:
db $00,$18,$E8,$18		;No left + right glitch for you
YSpeeds:
db $00,$18,$E8,$18
Frames:
db $17,$19,$1B,$1B
Carrying:
					LDA $13
					LSR #3
					AND #$03
					PHX
					TAX
					LDA Frames,x
					STA $13E0
					PLX
					;LDA #$19
					;STA $13E0
					LDA $190F,x
					AND #$7F
					STA $190F,x
					JSR SUB_GFX					;Draw the graphics
					STZ $7D
					STZ $7B
					REP #$20
					STZ $00
					STZ $02
					SEP #$20
					PHX
					LDA $15
					AND #$03
					TAX
					LDA XSpeeds,x
					STA $00
					LDA $15
					AND #$0C
					LSR #2
					TAX
					LDA YSpeeds,x
					STA $02
					PLX
					LDA $00
					BEQ .ZeroX
					BPL .PosX
					LDA $B6,x
					BPL +
					LDA $00
					CMP $B6,x
					BCS ++
					+
					DEC $B6,x
					++
					BRA .YSpeed
					.PosX
					LDA $B6,x
					BMI +
					LDA $00
					CMP $B6,x
					BEQ ++
					BCC ++
					+
					INC $B6,x
					++					
					BRA .YSpeed
					.ZeroX
					LDA $B6,x
					BEQ .YSpeed
					BPL +
					INC $B6,x
					BRA .YSpeed
					+
					DEC $B6,x
					.YSpeed
					
					LDA $02
					BEQ .ZeroY
					BPL .PosY
					LDA $AA,x
					BPL +
					LDA $02
					CMP $AA,x
					BCS ++
					+
					DEC $AA,x
					++
					BRA .Done
					.PosY
					LDA $AA,x
					BMI +
					LDA $02
					CMP $AA,x
					BEQ ++
					BCC ++
					+
					INC $AA,x
					++					
					BRA .Done
					.ZeroY
					LDA $AA,x
					BEQ .Done
					BPL +
					INC $AA,x
					BRA .Done
					+
					DEC $AA,x
					.Done
					JSL $019138
				
					
					LDA $77
					BIT #$03
					BEQ ++
					BIT #$01
					BNE +
					LDA $B6,x
					BPL ++
					STZ $B6,x
					BRA ++
					+
					LDA $B6,x
					BMI ++
					STZ $B6,x
					++
					
					LDA $77
					BIT #$0C
					BEQ ++
					BIT #$08
					BNE +
					LDA $AA,x
					BMI ++
					STZ $AA,x
					BRA ++
					+
					LDA $AA,x
					BPL ++
					STZ $AA,x
					++
					
					LDA $13E1
					BEQ ++
					BIT #$08
					BEQ +
					LDA $B6,x
					BMI ++
					STZ $B6,x
					BRA ++
					+
					LDA $B6,x
					BPL ++
					STZ $B6,x
					++
					
					JSL $01801A
					JSL $018022
					
					LDA $14E0,x
					XBA
					LDA $E4,x
					REP #$20
					STA $00
					SEP #$20
					LDA $1499
					BNE .Y
					LDA $76
					BNE .Right
					REP #$20
					LDA $00
					CLC
					ADC #$000B
					BRA .Store
					.Right
					REP #$20
					LDA $00
					SEC
					SBC #$000B
					.Store
					STA $94
					SEP #$20
					.Y
					LDA $19
					BEQ +
					LDA $14D4,x
					XBA
					LDA $D8,x
					REP #$20
					SEC
					SBC #$000D
					STA $96
					SEP #$20
					BRA ++
					+
					LDA $14D4,x
					XBA
					LDA $D8,x
					REP #$20
					SEC
					SBC #$000F
					STA $96
					SEP #$20
					++
					LDA $76
					STA !Direction,x
					RTS
Kicked:
					LDA #$08
					STA $14C8,x
					JMP return
					
JumpCarry:
					JMP Carrying
SPRITE_ROUTINE:	  	
					LDA $9D
					BNE return
					LDA $14C8,x
					CMP #$0B
					BEQ JumpCarry
					CMP #$09
					BEQ Kicked
					CMP #$08
					BNE return
					STZ $B6,x
					STZ $AA,x
					LDA $190F,x
					ORA #$80
					STA $190F,x
					JSL $019138
					JSL $01803A
					BCC +
					LDA $15
					BIT #$40
					BEQ +
					LDA $1470
                    ORA $187A
					BNE +
					LDA #$0B
					STA $14C8,x
					INC $1470
					+
					return:
					JSR SUB_OFF_SCREEN_X3
					JSR SUB_GFX					;Draw the graphics
                    RTS							;End the routine
					
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_HORZ_POS:		LDY #$00
					LDA $94
					SEC
					SBC $E4,x
					STA $0F
					LDA $95
					SBC $14E0,x
					BPL +
					INY
					+
					RTS

TileSize:
db $02,$00,$00

TilePointers:
dw Tiles1
dw Tiles2

XOff:
db $04,$FC,$FC

YOff:
db $00,$00,$08

Tiles1:
db $21,$20,$30
Tiles2:
db $21,$23,$33

SUB_GFX:            JSL !GetDrawInfo       	;Get all the drawing info, duh
					LDA !Direction,x
					STA $07
					PHX
					LDA $14
					LSR #2
					AND #$01
					ASL
					TAX
					REP #$20
					LDA TilePointers,x
					STA $05
					SEP #$20
					LDX #$02
					.GFXLoop
					
					LDA $07
					BEQ +
					LDA $00
					CLC
					ADC XOff,x
					BRA ++
					+
					LDA $00
					SEC
					SBC XOff,x
					CPX #$01
					BCC ++
					CLC
					ADC #$08
					++
					STA $0300,y
					
					LDA $01
					CLC
					ADC YOff,x
					STA $0301,y
					
					PHY
					TXY
					LDA ($05),y
					PLY
					STA $0302,y
					PHX
					LDX $15E9
					LDA $15F6,x
					PLX
					ORA $64
					PHX
					LDX $07
					BNE +
					ORA #$40
					+
					PLX
					STA $0303,y
					PHY
					TYA
					LSR #2
					TAY
					LDA TileSize,x
					PHX
					LDX $15E9
					ORA $15A0,x	; horizontal offscreen flag
					PLX
					STA $0460,y	;
					PLY
					INY
					INY
					INY
					INY
					DEX
					BPL .GFXLoop
					PLX
					LDA #$02
					LDY #$FF
					JSL $01B7B3					;/and then draw em
					EndIt:
					RTS
					
					
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_OFF_SCREEN
; This subroutine deals with sprites that have moved off screen
; It is adapted from the subroutine at $01AC0D
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
SPR_T12:             db $40,$B0
SPR_T13:             db $01,$FF
SPR_T14:             db $30,$C0,$A0,$C0,$A0,$F0,$60,$90		;bank 1 sizes
		            db $30,$C0,$A0,$80,$A0,$40,$60,$B0		;bank 3 sizes
SPR_T15:             db $01,$FF,$01,$FF,$01,$FF,$01,$FF		;bank 1 sizes
					db $01,$FF,$01,$FF,$01,$00,$01,$FF		;bank 3 sizes

SUB_OFF_SCREEN_X1:   LDA #$02                ; \ entry point of routine determines value of $03
                    BRA STORE_03            ;  | (table entry to use on horizontal levels)
SUB_OFF_SCREEN_X2:   LDA #$04                ;  | 
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X3:   LDA #$06                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X4:   LDA #$08                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X5:   LDA #$0A                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X6:  LDA #$0C                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X7:   LDA #$0E                ;  |
STORE_03:			STA $03					;  |            
					BRA START_SUB			;  |
SUB_OFF_SCREEN_X0:   STZ $03					; /

START_SUB:           JSR SUB_IS_OFF_SCREEN   ; \ if sprite is not off screen, return
                    BEQ RETURN_35           ; /
                    LDA $5B                 ; \  goto VERTICAL_LEVEL if vertical level
                    AND #$01                ; |
                    BNE VERTICAL_LEVEL      ; /     
                    LDA $D8,x               ; \
                    CLC                     ; | 
                    ADC #$50                ; | if the sprite has gone off the bottom of the level...
                    LDA $14D4,x             ; | (if adding 0x50 to the sprite y position would make the high byte >= 2)
                    ADC #$00                ; | 
                    CMP #$02                ; | 
                    BPL ERASE_SPRITE        ; /    ...erase the sprite
                    LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13
                    AND #$01
                    ORA $03
                    STA $01
                    TAY
                    LDA $1A
                    CLC
                    ADC SPR_T14,y
                    ROL $00
                    CMP $E4,x
                    PHP
                    LDA $1B
                    LSR $00
                    ADC SPR_T15,y
                    PLP
                    SBC $14E0,x
                    STA $00
                    LSR $01
                    BCC SPR_L31
                    EOR #$80
                    STA $00
SPR_L31:             LDA $00
                    BPL RETURN_35
ERASE_SPRITE:        LDA $14C8,x             ; \ if sprite status < 8, permanently erase sprite
                    CMP #$08                ; |
                    BCC KILL_SPRITE         ; /    
                    LDY $161A,x
                    CPY #$FF
                    BEQ KILL_SPRITE
                    LDA #$00
                    STA $1938,y
KILL_SPRITE:         STZ $14C8,x             ; erase sprite
RETURN_35:           RTS                     ; return

VERTICAL_LEVEL:      LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ; \
                    LSR A                   ; | 
                    BCS RETURN_35           ; /
                    LDA $E4,x               ; \ 
                    CMP #$00                ;  | if the sprite has gone off the side of the level...
                    LDA $14E0,x             ;  |
                    SBC #$00                ;  |
                    CMP #$02                ;  |
                    BCS ERASE_SPRITE        ; /  ...erase the sprite
                    LDA $13
                    LSR A
                    AND #$01
                    STA $01
                    TAY
                    LDA $1C
                    CLC
                    ADC SPR_T12,y
                    ROL $00
                    CMP $D8,x
                    PHP
                    LDA.w $001D 
                    LSR $00
                    ADC SPR_T13,y
                    PLP
                    SBC $14D4,x
                    STA $00
                    LDY $01
                    BEQ SPR_L38
                    EOR #$80
                    STA $00
SPR_L38:             
					LDA $00
                    BPL RETURN_35
                    BMI ERASE_SPRITE

SUB_IS_OFF_SCREEN:   LDA $15A0,x             ; \ if sprite is on screen, accumulator = 0 
                    ORA $186C,x             ; |  
                    RTS                     ; / return
