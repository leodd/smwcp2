;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; SMW Bowser Bowling Ball (sprite A1), by imamelia
;;
;; This is a disassembly of sprite A1 in SMW, Bowser's bowling ball.
;;
;; Uses first extra bit: YES
;;
;; If the extra bit is set, the sprite will detect ground.  If not, it will act like the
;; original and travel along hardcoded paths.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
incsrc subroutinedefs_xkas.asm
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; defines and tables
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!ExtraBit = $04

BowserBallXSpeed:
db $21,$DF

BowserBallYSpeed:
db $00,$00,$00,$00,$FE,$FC,$F8,$EC
db $EC,$EC,$E8,$E4,$E0,$DC,$D8,$D4
db $D0,$CC,$C8

BowserBallDispX:
db $F0,$00,$10,$F0,$00,$10,$F0,$00,$10,$00,$00,$00

BowserBallDispY:
db $E2,$E2,$E2,$F2,$F2,$F2,$02,$02,$02,$00,$00,$00

BowserBallTilemap:
db $29,$2B,$2D,$49,$4B,$4D,$69,$6B,$6D,$00,$00,$00

BowserBallTileProp:
db $0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$0F,$00,$00,$00

BowserBallTileSize:
db $02,$02,$02,$02,$02,$02,$02,$02,$02,$00,$00,$00


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR BowserBallMain
PLB
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BowserBallMain:

JSR BowserBallGFX	;

LDA $9D		; if sprites are locked...
BNE Return0	; return

JSR SubOffscreenX0	;
JSL $01A7DC	;
JSL $018022	;
JSL $01801A	;

LDA $AA,x	; if the sprite's Y speed
CMP #$40		; has not reached 40...
BPL MaxSpeed	;
CLC		;
ADC #$03	; make the sprite accelerate
BRA StoreSpeed	;

MaxSpeed:	;

LDA #$40		;

StoreSpeed:	;

STA $AA,x	;

LDA $AA,x	; if the sprite speed is negative...
BMI SkipSpeedSet	;
LDA $14D4,x	; or its Y position high byte is negative...
BMI SkipSpeedSet	; branch

LDA $7FAB10,x	;
AND #!ExtraBit	; if the extra bit is set...
BNE DetectGround	; make the sprite detect ground like a normal sprite

LDA $D8,x	; if not...
CMP #$B0		; check the sprite's Y position
BCC SkipSpeedSet	; don't set its X speed if the sprite Y position is less than B0
LDA #$B0		; if it is B0 or greater...
STA $D8,x	; make it stay at B0

Continue:		;

LDA $AA,x	; if the sprite Y speed
CMP #$3E		; is less than 3E...
BCC NoShake	; don't shake the ground

LDY #$25		;
STY $1DFC	; play a sound effect
LDY #$20		;
STY $1887	; shake the ground

NoShake:		;

CMP #$08		; if the sprite's Y speed is less than 08...
BCC NoSound2	; don't play the second sound effect

LDA #$01		;
STA $1DF9	;

NoSound2:	;

JSR HandleYSpeed	;

LDA $B6,x	; if the sprite's X speed is nonzero...
BNE SkipSpeedSet	; there is no need to set it again

JSR SubHorzPos		;
LDA BowserBallXSpeed,y	;
STA $B6,x

SkipSpeedSet:

LDA $B6,x	;
BEQ Return0	; return if the sprite's X speed is zero
BMI IncFrame	; increment the frame if the sprite's X speed is negative
DEC $1570,x	; decrement the frame if the sprite's X speed is positive
DEC $1570,x	;
IncFrame:		;
INC $1570,x	;

Return0:		;
RTS

DetectGround:	;

JSL $019138	; interact with objects

LDA $1588,x	;
AND #$04	;
BEQ SkipSpeedSet	;
BRA Continue	;

HandleYSpeed:	;

LDA $AA,x		;
STZ $AA,x		;
LSR			;
LSR			;
TAY			;
LDA BowserBallYSpeed,y	;
LDY $1588,x		;
BMI Return1		;
STA $AA,x		;

Return1:			;
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; graphics routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

BowserBallGFX:

JSL !GetDrawInfo		;

;LDA #$70			;
;STA $15EA,x		;

PHX			;
LDX #$09			; 12 tiles to draw

GFXLoop:			;

LDA $00			;
CLC			;
ADC BowserBallDispX,x	;
STA $0300,y		;

LDA $01			;
CLC			;
ADC BowserBallDispY,x	;
STA $0301,y		;

LDA BowserBallTilemap,x	;
STA $0302,y		;

LDA BowserBallTileProp,x	;
ORA $64			;
STA $0303,y		;

PHY			;
TYA			;
LSR #2			;
TAY			;
LDA BowserBallTileSize,x	;
STA $0460,y		;
PLY			;

INY #4			;
DEX			;
BPL GFXLoop		;

PLX			;
LDA #$09			;
LDY #$FF			;
JSL $81B7B3		;

RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; subroutines
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Table1:              db $0C,$1C
Table2:              db $01,$02
Table3:              db $40,$B0
Table6:              db $01,$FF
Table4:              db $30,$C0,$A0,$C0,$A0,$F0,$60,$90,$30,$C0,$A0,$80,$A0,$40,$60,$B0
Table5:              db $01,$FF,$01,$FF,$01,$FF,$01,$FF,$01,$FF,$01,$FF,$01,$00,$01,$FF

SubOffscreenX0:
LDA #$00
;BRA SubOffscreenMain
;SubOffscreenX1:
;LDA #$02
;BRA SubOffscreenMain
;SubOffscreenX2:
;LDA #$04
;BRA SubOffscreenMain
;SubOffscreenX3:
;LDA #$06
;BRA SubOffscreenMain
;SubOffscreenX4:
;LDA #$08
;BRA SubOffscreenMain
;SubOffscreenX5:
;LDA #$0A
;BRA SubOffscreenMain
;SubOffscreenX6:
;LDA #$0C
;BRA SubOffscreenMain
;SubOffscreenX7:
;LDA #$0E

SubOffscreenMain:

STA $03

JSR SubIsOffscreen
BEQ Return2

LDA $5B
LSR
BCS VerticalLevel
LDA $D8,x
CLC
ADC #$50
LDA $14D4,x
ADC #$00
CMP #$02
BPL EraseSprite
LDA $167A,x
AND #$04
BNE Return2
LDA $13
AND #$01
ORA $03
STA $01
TAY
LDA $1A
CLC
ADC Table4,y
ROL $00
CMP $E4,x
PHP
LDA $1B
LSR $00
ADC Table5,y
PLP
SBC $14E0,x
STA $00
LSR $01
BCC Label20
EOR #$80
STA $00
Label20:
LDA $00
BPL Return2

EraseSprite:
LDA $14C8,x
CMP #$08
BCC KillSprite
LDY $161A,x
CPY #$FF
BEQ KillSprite
LDA #$00
STA $1938,y
KillSprite:
STZ $14C8,x
Return2:
RTS

VerticalLevel:

LDA $167A,x
AND #$04
BNE Return2
LDA $13
LSR
BCS Return2
AND #$01
STA $01
TAY
LDA $1C
CLC
ADC Table3,y
ROL $00
CMP $D8,x
PHP
LDA $1D
LSR $00
ADC Table6,y
PLP
SBC $14D4,x
STA $00
LDY $02
BEQ Label22
EOR #$80
STA $00
Label22:
LDA $00
BPL Return2
BMI EraseSprite

SubIsOffscreen:
LDA $15A0,x
ORA $186C,x
RTS



SubHorzPos:

LDY #$00
LDA $94
SEC
SBC $E4,x
STA $0F
LDA $95
SBC $14E0,x
BPL $01
INY
RTS











