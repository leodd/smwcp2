!dryadState = !actorVar1,x
!dryadXPos = !actorVar2,x
!dryadYPos = !actorVar3,x
!dryadTimer = !actorVar4,x

SetSpeakerToDryad:
	LDX !dryadIndex
	STX !currentSpeaker
	RTL

DryadSetToNormal:
	LDX !dryadIndex
	LDA #$00
	STA !dryadState
	RTL

DryadSetToStationary:
	LDX !dryadIndex
	LDA #$01
	STA !dryadState
	RTL
	
DryadSetToMad:
	LDA #$CC
	STA !dryadBaseMouth
	RTL
	
DryadSetToFaceMario:
	LDA #$01
	STA !dryadFaceMario
	RTL
	
DryadSetToSpinning:
	STZ !dryadFaceMario
	RTL
	
DryadSetToConfident:
	LDA #$CE
	STA !dryadBaseMouth
	RTL
	
DryadSetToFrozen:
	LDX !dryadIndex
	LDA #$02
	STA !dryadState
	RTL
	
DryadCreate:
	REP #$20
	LDA.w #DryadINIT
	STA $00
	LDX.b #DryadINIT>>16
	STX $02
	
	LDA.w #DryadMAIN
	STA $03
	LDX.b #DryadMAIN>>16
	STX $05
	
	LDA.w #DryadNMI
	STA $06
	LDX.b #DryadNMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	STX !dryadIndex
	RTL

DryadINIT:
	STX !dryadIndex
	LDA #$0F
	STA !dryadYPos
	LDA #$6F
	STA !dryadXPos
	LDA #$CA
	STA !dryadBaseMouth
	RTL

DryadMAIN:
	
	PHK
	PLB

	INC !dryadBobCounter
	
	PHX				; Jump to the Doc's current routine.
	LDA !dryadState			;
	ASL
	TAX
	LDA DryadStateTable,x		;
	STA $00				;
	LDA DryadStateTable+1,x	;
	STA $01				;
	PLX				;
	PEA.w .ret1-1			;
	JMP [$0000]			;
.ret1	
	
	
	PHX
	LDA !dryadBobCounter
	AND #$3F
	TAX
	LDA DryadYSinTable,x
	PLX
	STA !dryadYPos
	
	JSR DryadDraw
	
	PHX
	
	LDA !dryadXPos
	STA $00
	LDX !marioIndex
	LDA !marioXPos
	STA $01
	
	LDA $00
	CLC
	ADC #$10
	CMP $01
	BCC .faceLeft
	JSL MarioSetToFacingRight
	BRA +
.faceLeft
	JSL MarioSetToFacingLeft
+
	PLX
	
	RTL

DryadNMI:
	RTL
	

DryadStateTable:
dw DryadNormal, DryadStationary, DryadFreezeEverything


DryadDraw:
	; mouth
	LDA !dryadXPos
	CLC
	ADC #$08
	STA $00
	LDA !dryadYPos
	CLC
	ADC #$08
	STA $01
	LDA #$36
	STA $03
	LDA !dryadMouthTile
	BEQ +
	STA $02
	LDA #$02
	JSL DrawTile
+	
	; body top left
	LDA !dryadXPos
	STA $00
	LDA !dryadYPos
	STA $01
	LDA !dryadTopLeftBodyTile
	STA $02
	LDA #$02
	JSL DrawTile
	
	; body top right
	LDA !dryadXPos
	CLC
	ADC #$10
	STA $00
	LDA !dryadYPos
	STA $01
	LDA !dryadTopRightBodyTile
	STA $02
	LDA #$02
	JSL DrawTile
	
	; body bottom left
	LDA !dryadXPos
	STA $00
	LDA !dryadYPos
	CLC
	ADC #$10
	STA $01
	LDA !dryadBotLeftBodyTile
	STA $02
	LDA #$02
	JSL DrawTile
	
	; body bottom right
	LDA !dryadXPos
	CLC
	ADC #$10
	STA $00
	LDA !dryadYPos
	CLC
	ADC #$10
	STA $01
	LDA !dryadBotRightBodyTile
	STA $02
	LDA #$02
	JSL DrawTile
	
	; plant left
	LDA !dryadXPos
	STA $00
	LDA !dryadYPos
	CLC
	ADC #$20
	STA $01
	LDA !dryadTopLeftPlantTile
	STA $02
	LDA #$02
	JSL DrawTile
	
	; plant right
	LDA !dryadXPos
	CLC
	ADC #$10
	STA $00
	LDA !dryadYPos
	CLC
	ADC #$20
	STA $01
	LDA !dryadTopLeftPlantTile
	CLC
	ADC #$02
	STA $02
	LDA #$02
	JSL DrawTile
	

	
	RTS

	
DryadFreezeEverything:
	DEC !dryadBobCounter
	LDA DryadBodySpinningTilesTopLeft
	STA !dryadTopLeftBodyTile
	LDA DryadBodySpinningTilesTopRight
	STA !dryadTopRightBodyTile
	LDA DryadBodySpinningTilesBotLeft
	STA !dryadBotLeftBodyTile
	LDA DryadBodySpinningTilesBotRight
	STA !dryadBotRightBodyTile
	JSR DryadDraw
	RTS
	
DryadBodySpinningTilesTopLeft:
	db $44, $82, $88, $C2
DryadBodySpinningTilesTopRight:
	db $46, $84, $8E, $C4
DryadBodySpinningTilesBotLeft:
	db $64, $A2, $A8, $E2
DryadBodySpinningTilesBotRight:
	db $66, $A4, $AE, $E4
	
DryadPlantSpinningTiles:
	db $08, $24

DryadXSinTable:
db $70, $73, $76, $79, $7C, $80, $83, $86, $88, $8B, $8E, $91, $94, $96, $99, $9B 
db $9D, $9F, $A1, $A3, $A5, $A7, $A8, $AA, $AB, $AC, $AD, $AE, $AF, $AF, $B0, $B0 
db $B0, $B0, $B0, $AF, $AF, $AE, $AD, $AC, $AB, $AA, $A8, $A7, $A5, $A3, $A1, $9F 
db $9D, $9B, $99, $96, $94, $91, $8E, $8B, $88, $86, $83, $80, $7C, $79, $76, $73 
db $70, $6D, $6A, $67, $64, $60, $5D, $5A, $58, $55, $52, $4F, $4C, $4A, $47, $45 
db $43, $41, $3F, $3D, $3B, $39, $38, $36, $35, $34, $33, $32, $31, $31, $30, $30 
db $30, $30, $30, $31, $31, $32, $33, $34, $35, $36, $38, $39, $3B, $3D, $3F, $41 
db $43, $45, $47, $4A, $4C, $4F, $52, $55, $58, $5A, $5D, $60, $64, $67, $6A, $6D 

DryadYSinTable:
db $10, $10, $10, $11, $11, $11, $11, $11, $11, $12, $12, $12, $12, $12, $12, $12 
db $12, $12, $12, $12, $12, $12, $12, $12, $11, $11, $11, $11, $11, $11, $10, $10 
db $10, $10, $10, $0F, $0F, $0F, $0F, $0F, $0F, $0E, $0E, $0E, $0E, $0E, $0E, $0E 
db $0E, $0E, $0E, $0E, $0E, $0E, $0E, $0E, $0F, $0F, $0F, $0F, $0F, $0F, $10, $10 

DryadNormal:
	PHX
	
	LDA !dryadBobCounter
	AND #$7F
	TAX
	LDA DryadXSinTable,x
	PLX
	PHX
	STA !dryadXPos
	
	LDA !dryadFaceMario
	BNE .faceMario
	
	
	LDA !dryadBobCounter
	LSR
	LSR
	AND #$03
	TAX
	BRA .nowSetTiles
.faceMario
	LDA !dryadBobCounter
	CLC
	ADC #$14
	LSR #5
	AND #$03
	TAX
;	LDA !dryadXPos
;	CMP #$40
;	BCC .faceToRight
;	CMP #$C0
;	BCS .faceToLeft
;	LDA !dryadBobCounter
;	ASL
;	BMI .faceFront
;.faceBack
;	LDX #$02
;	BRA .nowSetTiles
;.faceToRight
;	LDX #$01
;	BRA .nowSetTiles
;.faceToLeft
;	LDX #$03
;	BRA .nowSetTiles
;.faceFront
;	LDX #$00
	
	
.nowSetTiles
	LDA DryadBodySpinningTilesTopLeft,x
	STA !dryadTopLeftBodyTile
	LDA DryadBodySpinningTilesTopRight,x
	STA !dryadTopRightBodyTile
	LDA DryadBodySpinningTilesBotLeft,x
	STA !dryadBotLeftBodyTile
	LDA DryadBodySpinningTilesBotRight,x
	STA !dryadBotRightBodyTile
	
	TXA
	AND #$01
	TAX
	
	LDA DryadPlantSpinningTiles,x
	STA !dryadTopLeftPlantTile
	
	
	
	PLX
	RTS

DryadStationary:
	LDA !dryadStopMoving
	BNE ++
	JSR DryadNormal
	LDA !dryadBobCounter
	AND #$7F
	CMP #$35
	BNE +
	STA !dryadStopMoving
	STZ !dryadMouthTile
+
	RTS
	
	++
	
	LDA !textIsAppearing
	BEQ +
	LDA !dryadBobCounter
	ASL
	ASL
	AND #$20
+
	CLC
	ADC !dryadBaseMouth
	STA !dryadMouthTile
	RTS
	

	STZ !dryadMouthTile
	RTS

