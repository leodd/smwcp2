!YA = $06		; don't change
!NO = $05		;

org $00A5E7
	autoclean JML ASDF

freecode

ASDF:
	SEP #$20
	LDX $010B
	LDA.l Echo_Table,x
	STA $1DFD		; (requires smwcp2 AMK)
	REP #$20
	PLB
	LDX $0701
	JML $80A5EB

; !YA = enable SFX echo in this level; !NO = disable SFX echo
; Note that the song must enable echo in order for this to work (even if it doesn't use it)
;	#0	#1	#2	#3	#4	#5	#6	#7	#8	#9	#A	#B	#C	#D	#E	#F
Echo_Table:
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!YA,!NO,!NO	; level 000-00F
db !NO,!NO,!NO,!NO,!NO,!NO,!YA,!NO,!NO,!YA,!NO,!YA,!YA,!NO,!NO,!NO	; level 010-01F
db !NO,!YA,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!YA,!NO,!YA	; level 020-02F
db !NO,!NO,!NO,!YA,!NO,!NO,!NO,!NO,!NO,!YA,!NO,!NO,!YA,!NO,!YA,!NO	; level 030-03F
db !NO,!NO,!NO,!YA,!NO,!NO,!NO,!NO,!NO,!NO,!YA,!NO,!NO,!NO,!NO,!YA	; level 040-04F
db !NO,!NO,!NO,!NO,!YA,!NO,!NO,!NO,!YA,!YA,!YA,!NO,!NO,!NO,!NO,!NO	; level 050-05F
db !NO,!NO,!NO,!NO,!NO,!NO,!YA,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!YA	; level 060-06F
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 070-07F
db !NO,!NO,!YA,!NO,!NO,!NO,!YA,!NO,!YA,!YA,!NO,!NO,!NO,!NO,!NO,!NO	; level 080-08F
db !NO,!YA,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!YA,!NO,!NO,!NO,!YA,!NO	; level 090-09F
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!YA,!NO,!NO,!NO,!NO,!NO	; level 0A0-0AF
db !YA,!NO,!NO,!NO,!YA,!YA,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 0B0-0BF
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 0C0-0CF
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 0D0-0DF
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!YA,!NO,!NO,!YA,!NO,!NO	; level 0E0-0EF
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 0F0-0FF
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 100-10F
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 110-11F
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 120-12F
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 130-13F
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!YA	; level 140-14F
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!YA,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 150-15F
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 160-16F
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 170-17F
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 180-18F
db !YA,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!YA,!YA,!NO	; level 190-19F
db !YA,!YA,!YA,!YA,!NO,!YA,!NO,!YA,!NO,!NO,!YA,!YA,!YA,!NO,!NO,!NO	; level 1A0-1AF
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 1B0-1BF
db !NO,!NO,!NO,!NO,!NO,!NO,!YA,!NO,!NO,!NO,!YA,!NO,!NO,!NO,!NO,!NO	; level 1C0-1CF
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!YA,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 1D0-1DF
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 1E0-1EF
db !NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO,!NO	; level 1F0-1FF
;	#0	#1	#2	#3	#4	#5	#6	#7	#8	#9	#A	#B	#C	#D	#E	#F