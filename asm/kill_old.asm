HEADER
LOROM


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Start Status bar removal
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ORG $0081F4
	NOP #3

ORG $008293
	db $00

ORG $0082E8
	NOP #3

ORG $008C89
	NOP #955
	
ORG $00A2D5
	NOP #3
	
ORG $00A5D5
	NOP #3

ORG $009051
	NOP #40

ORG $00985A
	NOP #3

ORG $00A5A8
	NOP #3

ORG $008F98
	NOP #3
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Start RAM freeing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
ORG $0091BD
	NOP #9
	
ORG $009A91
	NOP #3
	
ORG $00D0E8
	NOP #9
	
ORG $01F10E
	NOP #6
	
ORG $058583
	NOP #13
	
ORG $028758
	NOP #27
	
ORG $02AE12
	NOP #35
	
ORG $05CEF6
	NOP #15
	
ORG $009E4B
	NOP #9
	
ORG $05CF1B
	NOP #3
	
ORG $02AA4A
	STA $0F5E,X
ORG $02AA86
	STA $0F5E,X
ORG $02AAEC
	STA $0F5E,X
ORG $02AB3F
	STA $0F5E,X
ORG $02DF76
	STA $0F5E,Y
ORG $02F91C
	LDA $0F5E,X
ORG $02F925
	DEC $0F5E,X
ORG $02F9B5
	LDA $0F5E,X
ORG $02FA28
	INC $0F5E,X
ORG $02FA42
	LDA $0F5E,X
ORG $02FAA8
	LDA $0F5E,X
ORG $02FCF8
	LDA $0F5E,X
ORG $02FD0D
	INC $0F5E,X
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Start old 1-up handle removal
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ORG $028AB4
	NOP #33	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Start smoke edit
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
ORG $00FBA4				
	db $64,$64,$62,$60,$E8,$EA,$EC,$EA
ORG $01E985						
	db $64,$64,$62,$60
ORG $028C6A					
	db $64,$64,$62,$60
ORG $028D42				
	db $68,$68,$6A,$6A,$6A,$62,$62,$62
	db $64,$64,$64,$64,$64
ORG $0296D8					
	db $64,$62,$64,$62,$60,$62,$60
ORG $029922					
	db $64,$62,$64,$62,$62
ORG $029C33				
	db $64,$64,$62,$60,$60,$60,$60,$60
ORG $02A347					
	db $64,$64,$60,$62

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Start score removal
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ORG $02ADBD
	JML disable_score
	
ORG $00F9F5
	disable_score:
		LDA $16E1,x
		CMP #$0D
		BCC no_1up
		JML $82ADC2
	no_1up:
		JML $82ADC5
		
		
ORG $05CC42
	db $FC,$38,$FC,$38,$FC,$38,$FC,$38
	db $FC,$38,$FC,$38,$FC,$38,$FC,$38
	db $FC,$38,$FC,$38,$FC,$38,$FC,$38
	db $FC,$38,$FC,$38,$FC,$38,$FF
		
ORG $05CC77
	BRA +
	NOP #11
	+
	
ORG $05CCAA
	BRA +
	NOP #76
	+

ORG $05CE4C
	JMP +
	NOP #83
	+
	
ORG $05CEAF
	db $FC
	
ORG $05CCFB
	BRA +
	NOP #41
	+
	
ORG $05CECF
	BRA +
	NOP #52
	+

ORG $05CF36
	NOP #3

ORG $05CF78
	BRA +
	NOP #38
	+
	
ORG $05CDFD
	JMP +
	NOP #57
	+

ORG $028008
RTL

