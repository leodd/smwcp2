; Overworld SMWC Coin graphics - Patch by RPG Hacker

print ""
print "Overworld SMWC Coins"


header
lorom


!XPos = $12
!YPos = $29

!FrameTile1 = $4C
!FrameTile2 = $4D

!FilledTile = $6E
!EmptyTile = $6C



;;;;;;;;;;;;;;;;
;External files;
;;;;;;;;;;;;;;;;


incsrc CoinDefines.asm		; Coin defines



;;;;;;;;;
;Defines;
;;;;;;;;;


; Only touch when you know what you're doing
; Used for calculating translevel number and for timer

!owram = $0DC3		; 4 Bytes
!timer = !owram

!timerval = $10		; Timer for the blinking coin



;;;;;;;;;;;;;;;;;
;Routine hijacks;
;;;;;;;;;;;;;;;;;

org $00C496
	autoclean JSL save_coins_secret
	NOP
	NOP

org $00C918
	autoclean JSL save_coins_normal
	NOP
	NOP

org $048356			; OW Routine that runs each frame
	autoclean jml DisplayCoins
	nop
	
org $0491E0
	autoclean JSL load_coins
	NOP

;;;;;;;;;;;;;
;PATCH START;
;;;;;;;;;;;;;

freecode

;;;;;;;;;;;;;;;;;
;Display routine;
;;;;;;;;;;;;;;;;;

load_coins:
	LDA #$80
	STA $1DFB
	
	PHB
	PHK
	PLB
	
	STZ $00
	LDA $13BF
	LSR
	LSR
	LSR
	TAX
	LDA $13BF
	AND #$07
	TAY
	
	LDA !ramspace,x
	AND levelbittable,y
	BEQ +
	INC $00
+	LDA !ramspace+11,x
	AND levelbittable,y
	BEQ +
	LDA #$02
	TSB $00
+	LDA !ramspace+22,x
	AND levelbittable,y
	BEQ +
	LDA #$04
	TSB $00
+	LDA $13BF
	CMP !TempCounter
	BNE +
	
	LDA !TempCounter+1
	TSB $00
	LSR
	LSR
	LSR
	TSB $00
	
+	LDA $00
	STA !TempCounter+1
	PLB
	RTL
	
save_coins_secret:
	JSR save_coins
	LDX $1435
	LDA $1433
	RTL
	
save_coins_normal:
	JSR save_coins
	STZ $18C2
	STZ $13CE
	RTL
	
save_coins:
	LDA !TempCounter+1
	BEQ .no_coins
	
	PHB
	PHK
	PLB
	
	LDA $13BF
	LSR
	LSR
	LSR
	TAX
	LDA $13BF
	AND #$07
	TAY
	
	LDA !TempCounter+1
	LSR
	BCC +
	
	LDA levelbittable,y
	ORA !ramspace,x
	STA !ramspace,x
	
+	LDA !TempCounter+1
	LSR
	LSR
	BCC +
	
	LDA levelbittable,y
	ORA !ramspace+11,x
	STA !ramspace+11,x	
	
+	LDA !TempCounter+1
	LSR
	LSR
	LSR
	BCC +
	
	LDA levelbittable,y
	ORA !ramspace+22,x
	STA !ramspace+22,x
	
+	LDA #$00
	STA !TempCounter+1
	PLB
	
.no_coins
	RTS
	
DisplayCoins:
	phy
	phx
	pha
	phb
	phk
	plb
	php

	LDA $13D9	; only run when standing on a level tile and not pausing
	SEC : SBC #$03
	ORA $13D4
	BEQ +
	JMP .return
	+

.begin
	REP #$20				;$049912	|
	LDX.w $0DD6				;$049914	|
	LDA.w $1F17,X				;$049917	|
	LSR					;$04991A	|
	LSR					;$04991B	|
	LSR					;$04991C	|
	LSR					;$04991D	|
	STA $00					;$04991E	|
	STA.w $1F1F,X				;$049920	|
	LDA.w $1F19,X				;$049923	|
	LSR					;$049926	|
	LSR					;$049927	|
	LSR					;$049928	|
	LSR					;$049929	|
	STA $02					;$04992A	|
	STA.w $1F21,X				;$04992C	|
	TXA					;$04992F	|
	LSR					;$049930	|
	LSR					;$049931	|
	TAX					;$049932	|
	PHK
	PEA.w (+)-1
	PEA $8574
	JML $849885
+	REP #$10				;$049936	|
	LDX $04					;$049938	|
	LDA.l $7ED000,X				;$04993A	|
	AND.w #$00FF				;$04993E	|
	SEP #$30
	STA $13BF
	lsr
	lsr
	lsr
	tay
	lda $13BF
	and #$07
	tax
	lda leveltable,y
	and levelbittable,x
	cmp #$00
	bne .coinlevel
	jmp .nocoins


.coinlevel

	JSR .showFrame

	lda !timer	; Increase timer
	cmp.b #!timerval*2
	bcc .timerok
	lda #$FF

.timerok
	inc
	sta !timer



.coin1
	lda $13BF	; Is first coin collected?
	and #$07
	tax
	lda levelbittable,x
	sta.l !TempCounter+3

	lda $13BF
	lsr #3
	tax

	lda.l !ramspace,x
	and.l !TempCounter+3
	beq .1tempcheck
	bra .1filled

.1tempcheck
	lda $13BF	; Is first coin temporarily collected?
	cmp.l !TempCounter
	bne .1empty

	lda.l !TempCounter+1
	lsr #3
	eor.l !TempCounter+2
	and #$01
	beq .1empty

	lda !timer
	cmp.b #!timerval
	bcc .1empty
	bra .1filled	

.1filled
	JSR .firstFilled		;[PUT GRAPHICS CODE FOR FIRST FILLED COIN HERE]

	bra .coin2

.1empty
	JSR .firstEmpty			;[PUT GRAPHICS CODE FOR FIRST EMPTY COIN HERE]



.coin2
	lda $13BF	; Is second coin collected?
	and #$07
	tax
	lda levelbittable,x
	sta.l !TempCounter+3

	lda $13BF
	lsr #3
	tax

	lda.l !ramspace+$0B,x
	and.l !TempCounter+3
	beq .2tempcheck
	bra .2filled

.2tempcheck
	lda $13BF	; Is second coin temporarily collected?
	cmp.l !TempCounter
	bne .2empty

	lda.l !TempCounter+1
	lsr #3
	eor.l !TempCounter+2
	and #$02
	beq .2empty

	lda !timer
	cmp.b #!timerval
	bcc .2empty
	bra .2filled	

.2filled
	JSR .secondFilled		;[PUT GRAPHICS CODE FOR SECOND FILLED COIN HERE]

	bra .coin3

.2empty
	JSR .secondEmpty		;[PUT GRAPHICS CODE FOR SECOND EMPTY COIN HERE]



.coin3
	lda $13BF	; Is third coin collected?
	and #$07
	tax
	lda levelbittable,x
	sta.l !TempCounter+3

	lda $13BF
	lsr #3
	tax

	lda.l !ramspace+$16,x
	and !TempCounter+3
	beq .3tempcheck
	bra .3filled

.3tempcheck
	lda $13BF	; Is third coin temporarily collected?
	cmp.l !TempCounter
	bne .3empty

	lda.l !TempCounter+1
	lsr #3
	eor.l !TempCounter+2
	and #$04
	beq .3empty

	lda !timer
	cmp.b #!timerval
	bcc .3empty
	bra .3filled	

.3filled
	JSR .thirdFilled		;[PUT GRAPHICS CODE FOR THIRD FILLED COIN HERE]

	bra .return

.3empty
	JSR .thirdEmpty			;[PUT GRAPHICS CODE FOR THIRD EMPTY COIN HERE]

	bra .return



.nocoins
	;[PUT CODE FOR WHEN ON LEVEL WITHOUT ANY COINS]



.return
	plp
	plb
	pla
	plx
	ply

	lda $13D9               
	cmp #$03 
	JML $84835B|$800000



.showFrame

	PHY
	LDY #$02
	LDX #$4C
-
	LDA .frameXPos,y
	STA $0200,x
	LDA #!YPos
	STA $0201,x
	LDA .frameTilemap,y
	STA $0202,x
	LDA .frameProperties,y
	STA $0203,x
	PHX : TXA : LSR #2 : TAX
	LDA #$02
	STA $0420,x
	PLX

	INX #4
	DEY
	BPL -

	PLY
	RTS

.frameXPos
	db !XPos,!XPos+8,!XPos+18
.frameTilemap
	db !FrameTile1,!FrameTile2,!FrameTile1
.frameProperties
	db $30,$30,$70










.firstFilled
	LDX #$40
	LDA.b #!XPos+4
	JSR .shared1
	RTS

.firstEmpty
	LDX #$40
	LDA.b #!XPos+4
	JSR .shared2
	RTS



.secondFilled
	LDX #$44
	LDA.b #!XPos+13
	JSR .shared1
	RTS

.secondEmpty
	LDX #$44
	LDA.b #!XPos+13
	JSR .shared2
	RTS



.thirdFilled
	LDX #$48
	LDA.b #!XPos+22
	JSR .shared1
	RTS

.thirdEmpty
	LDX #$48
	LDA.b #!XPos+22
	JSR .shared2
	RTS



.emptyTilemap
	db !EmptyTile,!EmptyTile+1,!EmptyTile,!EmptyTile+1
.emptyProperties
	db $30,$30,$70,$B0




.shared1
	STA $0200,x
	LDA.b #!YPos+4
	STA $0201,x
	LDA #!FilledTile
	STA $0202,x
	LDA #$30
	BRA .shared3

.shared2
	STA $0200,x
	LDA.b #!YPos+4
	STA $0201,x
	LDA $13 : LSR #3 : AND #$03 : PHX : TAX
	LDA .emptyTilemap,x
	PLX
	STA $0202,x
	LDA $13 : LSR #3 : AND #$03 : PHX : TAX
	LDA .emptyProperties,x
	PLX
.shared3
	STA $0203,x
	TXA : LSR #2 : TAX
	STZ $0420,x
	RTS



;;;;;;;;;;;;;;
;Patch tables;
;;;;;;;;;;;;;;


levelbittable:
	db $80,$40,$20,$10,$08,$04,$02,$01

; This table defines, which levels do and which don't have SMWC coins
; Order is from left to right

leveltable:
	db %00111111	; Level 000 - 007
	db %11111111	; Level 008 - 00F
	db %11111111	; Level 010 - 017
	db %11111111	; Level 018 - 01F
	db %11111111	; Level 020 - 024, Level 101 - 103
	db %11111111	; Level 104 - 10B
	db %11111111	; Level 10C - 113
	db %11111111	; Level 114 - 11B
	db %11111111	; Level 11C - 123
	db %11111111	; Level 124 - 12B
	db %11111100	; Level 12C - 133
	db %00000000	; Level 134 - 13B




print freespaceuse," bytes written at an address you don't need to care about."
print ""