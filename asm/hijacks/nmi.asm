ORG $008176
	%clean("JML nmi_hijack")
	NOP #2

%origin(!nmi_freespace)
nmi_hijack:
	LDA $4210
	NOP #4		; jsl goes here
	JSR nmi_code
	LDA $1DFB
	JML $80817C

; ; also does not push P
; !preserve_dp = 1

; org $0082BC
	; REP #$30
	; if !preserve_dp
		; PLD
		; PLB
		; PLY
		; PLX
		; PLA
	; else
		; PLB
		; PLY
		; PLX
		; PLA
		; PLP
	; endif
		; RTI
	; warnpc $0082C3

; org $00BA4E			; skip sei php at nmi start
	; if !preserve_dp
		; JML $80816C
	; else
		; JML $80816B
	; endif

; %origin(!nmi_freespace)
; nmi_hijack:
	; LDA $4210
	; ;NOP #4		; jsl goes here
	; if !preserve_dp
		; PHD		; push DP and set to 0
		; LDA #$0000
		; TCD
	; endif
	; JSR nmi_code
	; LDA $1DFB
	; JML $80817C